package com.m2msolutions.m2mcomputadordebordo.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class VehicleStatus {
	
	@DatabaseField(generatedId=true)
	private Long id;
	
	@DatabaseField()
	private Long requestedId;
	
	@DatabaseField()
	private Double latitude;
	
	@DatabaseField()
	private Double longitude;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Long getRequestedId() {
		return requestedId;
	}

	public void setRequestedId(Long requestedId) {
		this.requestedId = requestedId;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

}
