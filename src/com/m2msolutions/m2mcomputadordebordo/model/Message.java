package com.m2msolutions.m2mcomputadordebordo.model;

import java.util.Date;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Message {

	@DatabaseField(generatedId=true)
	private Long id;
	
	@DatabaseField
	private Date date;
	
	@DatabaseField
	private MessageOwner messageOwner;
	
	@DatabaseField
	private String message;
	
	@DatabaseField
	private String serverId;
	
	@DatabaseField
	private Boolean sent;
	
	public Boolean isSent() {
		return sent;
	}

	public void setSent(Boolean sent) {
		this.sent = sent;
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getServerId() {
		return serverId;
	}
	
	public void setServerId(String serverId) {
		this.serverId = serverId;
	}
	
	public MessageOwner getMessageOwner() {
		return messageOwner;
	}
	
	public void setMessageOwner(MessageOwner messageOwner) {
		this.messageOwner = messageOwner;
	}
	
}
