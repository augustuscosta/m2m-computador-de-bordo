package com.m2msolutions.m2mcomputadordebordo.model;

public enum NivelTemperatura {
	
	BAIXA(0),
	MEDIA(1),
	ALTA(2);
	
	private int value;
	
	private NivelTemperatura(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

}
