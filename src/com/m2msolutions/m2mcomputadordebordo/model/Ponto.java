package com.m2msolutions.m2mcomputadordebordo.model;

import org.simpleframework.xml.Element;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Ponto {
	
	
	public Ponto(){
		
	}
	
	@Element
	@DatabaseField(id = true)
	private Long id;
	
	
	@Element
	@DatabaseField(columnName="id_rota")
	private Long idRota;
	
	@Element
	@DatabaseField
	private Long indice;
	
	@Element
	@DatabaseField
	private Long tempoMedio;
	
	@Element
	@DatabaseField
	private String nome;
	
	@Element
	@DatabaseField
	private Double latitude;
	
	@Element
	@DatabaseField
	private Double longitude;
	
	@DatabaseField
	@Element()	
	private Double fraction;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdRota() {
		return idRota;
	}

	public void setIdRota(Long idRota) {
		this.idRota = idRota;
	}

	public Long getIndice() {
		return indice;
	}

	public void setIndice(Long indice) {
		this.indice = indice;
	}

	public Long getTempoMedio() {
		return tempoMedio;
	}

	public void setTempoMedio(Long tempoMedio) {
		this.tempoMedio = tempoMedio;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getFraction() {
		return fraction;
	}

	public void setFraction(Double fraction) {
		this.fraction = fraction;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((fraction == null) ? 0 : fraction.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((idRota == null) ? 0 : idRota.hashCode());
		result = prime * result + ((indice == null) ? 0 : indice.hashCode());
		result = prime * result
				+ ((latitude == null) ? 0 : latitude.hashCode());
		result = prime * result
				+ ((longitude == null) ? 0 : longitude.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result
				+ ((tempoMedio == null) ? 0 : tempoMedio.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ponto other = (Ponto) obj;
		if (fraction == null) {
			if (other.fraction != null)
				return false;
		} else if (!fraction.equals(other.fraction))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (idRota == null) {
			if (other.idRota != null)
				return false;
		} else if (!idRota.equals(other.idRota))
			return false;
		if (indice == null) {
			if (other.indice != null)
				return false;
		} else if (!indice.equals(other.indice))
			return false;
		if (latitude == null) {
			if (other.latitude != null)
				return false;
		} else if (!latitude.equals(other.latitude))
			return false;
		if (longitude == null) {
			if (other.longitude != null)
				return false;
		} else if (!longitude.equals(other.longitude))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (tempoMedio == null) {
			if (other.tempoMedio != null)
				return false;
		} else if (!tempoMedio.equals(other.tempoMedio))
			return false;
		return true;
	}

	
}
