package com.m2msolutions.m2mcomputadordebordo.model;

import org.simpleframework.xml.Element;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Motorista {
	
	public Motorista(){
		super();
	}
	
	
	public Motorista(Long id, String nome, String matricula, String rfId){
		this.id = id;
		this.nome = nome;
		this.matricula = matricula;
		this.rfId = rfId;
	}
	
	@DatabaseField(id = true)
	@Element
	private Long id;
	
	@DatabaseField()
	@Element
	private String nome;
	
	@DatabaseField()
	@Element
	private String senha;
	
	@DatabaseField()
	@Element
	private String matricula;
	
	@DatabaseField()
	@Element
	private String rfId;
	
	private Rota rotaSelecionada;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getRfId() {
		return rfId;
	}

	public void setRfId(String rfId) {
		this.rfId = rfId;
	}


	public Rota getRotaSelecionada() {
		return rotaSelecionada;
	}


	public void setRotaSelecionada(Rota rotaSelecionada) {
		this.rotaSelecionada = rotaSelecionada;
	}

}
