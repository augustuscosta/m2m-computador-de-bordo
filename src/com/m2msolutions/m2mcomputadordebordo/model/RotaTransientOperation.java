package com.m2msolutions.m2mcomputadordebordo.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
			
@DatabaseTable
public class RotaTransientOperation {
	
	@DatabaseField(generatedId =true)
	private long id;
	
	@DatabaseField(foreign = true)
	private Rota rota;
	
	@DatabaseField
	private long rotaID;
	
	public long getRotaID() {
		return rotaID;
	}

	public void setRotaID(long rotaID) {
		this.rotaID = rotaID;
	}

	@DatabaseField
	private boolean initOrFinish;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Rota getRota() {
		return rota;
	}

	public void setRota(Rota rota) {
		this.rota = rota;
	}

	public boolean isInitOrFinish() {
		return initOrFinish;
	}

	public void setInitOrFinish(boolean initOrFinish) {
		this.initOrFinish = initOrFinish;
	}

}
