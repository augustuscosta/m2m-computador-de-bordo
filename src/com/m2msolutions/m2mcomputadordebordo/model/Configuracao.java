package com.m2msolutions.m2mcomputadordebordo.model;

import org.simpleframework.xml.Element;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Configuracao {
	
	@DatabaseField(id = true)
	@Element
	private Long id;
	
	@DatabaseField()
	@Element
	private NivelTemperatura nivelTemperatura;
	
	@DatabaseField()
	@Element
	private Double velocidadeMaxima;
	
	@DatabaseField()
	@Element
	private Boolean velocidade;
	
	@DatabaseField()
	@Element
	private String masterPassword;
	
	@DatabaseField()
	@Element
	private Long configTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public NivelTemperatura getNivelTemperatura() {
		return nivelTemperatura;
	}

	public void setNivelTemperatura(NivelTemperatura nivelTemperatura) {
		this.nivelTemperatura = nivelTemperatura;
	}

	public Double getVelocidadeMaxima() {
		return velocidadeMaxima;
	}

	public void setVelocidadeMaxima(Double velocidadeMaxima) {
		this.velocidadeMaxima = velocidadeMaxima;
	}

	public Boolean getVelocidade() {
		return velocidade;
	}

	public void setVelocidade(Boolean velocidade) {
		this.velocidade = velocidade;
	}

	public String getMasterPassword() {
		return masterPassword;
	}

	public void setMasterPassword(String masterPassword) {
		this.masterPassword = masterPassword;
	}

	public Long getConfigTime() {
		return configTime;
	}

	public void setConfigTime(Long configTime) {
		this.configTime = configTime;
	}
	
}
