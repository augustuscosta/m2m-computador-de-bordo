package com.m2msolutions.m2mcomputadordebordo.model;

import org.simpleframework.xml.Element;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class MensagemPre {

	@Element
	@DatabaseField(id=true)
	private Long id;
	
	@Element
	@DatabaseField
	private String message;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
