package com.m2msolutions.m2mcomputadordebordo.model.xmlrootobjects;

import java.util.ArrayList;
import java.util.List;

import org.simpleframework.xml.ElementList;

import com.m2msolutions.m2mcomputadordebordo.model.MensagemPre;

public class MensagensPre {
	
	@ElementList
	public List<MensagemPre> listaMensagensPre = new ArrayList<MensagemPre>(); 

}
