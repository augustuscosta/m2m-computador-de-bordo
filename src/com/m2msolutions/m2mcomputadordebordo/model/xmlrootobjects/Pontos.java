package com.m2msolutions.m2mcomputadordebordo.model.xmlrootobjects;

import java.util.ArrayList;
import java.util.List;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import com.m2msolutions.m2mcomputadordebordo.model.Ponto;

@Root
public class Pontos {
	
	@ElementList
	public List<Ponto> listaPontos = new ArrayList<Ponto>();

}
