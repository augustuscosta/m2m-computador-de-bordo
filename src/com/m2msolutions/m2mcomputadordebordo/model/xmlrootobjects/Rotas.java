package com.m2msolutions.m2mcomputadordebordo.model.xmlrootobjects;

import java.util.ArrayList;
import java.util.List;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import com.m2msolutions.m2mcomputadordebordo.model.Rota;

@Root
public class Rotas {
	
	@ElementList
	public List<Rota> listaRotas = new ArrayList<Rota>();

}
