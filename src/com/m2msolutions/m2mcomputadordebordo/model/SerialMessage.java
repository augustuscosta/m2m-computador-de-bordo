package com.m2msolutions.m2mcomputadordebordo.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class SerialMessage {
	
	
	@DatabaseField(generatedId = true)
	private Long id;

	@DatabaseField()
	private String hexCommand;
	
	@DatabaseField()
	private Integer sent;
	
	@DatabaseField(foreign = true, foreignAutoRefresh = true, foreignAutoCreate = true)
	private Message message;
	
	@DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
	private RotaTransientOperation rTO;
	

	public Integer getSent() {
		return sent;
	}

	public void setSent(Integer sent) {
		this.sent = sent;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getHexCommand() {
		return hexCommand;
	}

	public void setHexCommand(String hexCommand) {
		this.hexCommand = hexCommand;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public RotaTransientOperation getrTO() {
		return rTO;
	}

	public void setrTO(RotaTransientOperation rTO) {
		this.rTO = rTO;
	}

}
