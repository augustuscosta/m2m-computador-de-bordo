package com.m2msolutions.m2mcomputadordebordo.model;

import com.m2msolutions.m2mcomputadordebordo.R;

public enum MessageOwner {
	
	MOTORISTA(R.string.motorista),
	OPERDADOR(R.string.operador);
	
	private int value;

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	 MessageOwner(int value) {
		this.value = value;
	}
	
	public static MessageOwner fromValue(int value){
		for (MessageOwner messageOwner : MessageOwner.values()) {
			if(messageOwner.getValue() == value) return messageOwner;
		}
		
		return null;
	}
	
	public static MessageOwner fromName(String name){
		for (MessageOwner messageOwner : MessageOwner.values()) {
			if(messageOwner.name().equals(name)) return messageOwner;
		}
		
		return null;
	}

}
