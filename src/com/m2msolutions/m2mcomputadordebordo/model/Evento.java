package com.m2msolutions.m2mcomputadordebordo.model;

import org.simpleframework.xml.Element;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Evento {
	
	@DatabaseField(id = true)
	@Element
	private Long id;
	
	@DatabaseField
	@Element
	private AlertaTipo alertaTipo;
	
	@DatabaseField
	@Element
	private String texto;
	
	@DatabaseField
	@Element
	private String mp3Path;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AlertaTipo getAlertaTipo() {
		return alertaTipo;
	}

	public void setAlertaTipo(AlertaTipo alertaTipo) {
		this.alertaTipo = alertaTipo;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public String getMp3Path() {
		return mp3Path;
	}

	public void setMp3Path(String mp3Path) {
		this.mp3Path = mp3Path;
	}

}
