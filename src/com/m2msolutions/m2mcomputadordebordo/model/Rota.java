package com.m2msolutions.m2mcomputadordebordo.model;

import java.util.Date;
import java.util.List;

import org.simpleframework.xml.Element;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Rota {
	
	
	public Rota(){
		super();
	}
	
	public Rota(Long id, String nome, Double totalKm, Long totalTempo){
		this.id = id;
		this.nome = nome;
		this.totalKm = totalKm;
		this.totalTempo = totalTempo;
	}
	
	@DatabaseField(id = true)
	@Element
	private Long id;
	
	@DatabaseField()
	@Element
	private String nome;
	
	@DatabaseField()
	@Element
	private Double totalKm;
	
	@DatabaseField()
	@Element
	private Long totalTempo;
		
	@DatabaseField
	private Boolean emCurso;
	
	@DatabaseField
	private Date startedAt;
	
	@DatabaseField(foreign= true,foreignAutoCreate=true, foreignAutoRefresh=true,foreignColumnName="id")
	private Ponto lastPonto;
	
	private List<Ponto> pontos;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Double getTotalKm() {
		return totalKm;
	}

	public void setTotalKm(Double totalKm) {
		this.totalKm = totalKm;
	}

	public Long getTotalTempo() {
		return totalTempo;
	}

	public void setTotalTempo(Long totalTempo) {
		this.totalTempo = totalTempo;
	}

	public Date getStartedAt() {
		return startedAt;
	}

	public void setStartedAt(Date startedAt) {
		this.startedAt = startedAt;
	}

	public Ponto getLastPonto() {
		return lastPonto;
	}

	public void setLastPonto(Ponto lastPonto) {
		this.lastPonto = lastPonto;
	}

	public List<Ponto> getPontos() {
		return pontos;
	}

	public void setPontos(List<Ponto> pontos) {
		this.pontos = pontos;
	}

	public Boolean getEmCurso() {
		return emCurso;
	}

	public void setEmCurso(Boolean emCurso) {
		this.emCurso = emCurso;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((pontos == null) ? 0 : pontos.hashCode());
		result = prime * result + ((totalKm == null) ? 0 : totalKm.hashCode());
		result = prime * result
				+ ((totalTempo == null) ? 0 : totalTempo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rota other = (Rota) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (pontos == null) {
			if (other.pontos != null)
				return false;
		} else if (!pontos.equals(other.pontos))
			return false;
		if (totalKm == null) {
			if (other.totalKm != null)
				return false;
		} else if (!totalKm.equals(other.totalKm))
			return false;
		if (totalTempo == null) {
			if (other.totalTempo != null)
				return false;
		} else if (!totalTempo.equals(other.totalTempo))
			return false;
		return true;
	}
	
	
}
