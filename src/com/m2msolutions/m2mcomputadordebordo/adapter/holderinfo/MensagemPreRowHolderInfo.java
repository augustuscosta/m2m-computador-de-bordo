package com.m2msolutions.m2mcomputadordebordo.adapter.holderinfo;

import android.widget.TextView;

public class MensagemPreRowHolderInfo {

	public TextView message;

	public void drawRow(String msg) {
		if (msg == null) {
			return;
		}

		message.setText(msg);
	}
}
