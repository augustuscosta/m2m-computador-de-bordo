package com.m2msolutions.m2mcomputadordebordo.adapter.holderinfo;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.m2msolutions.m2mcomputadordebordo.R;
import com.m2msolutions.m2mcomputadordebordo.model.TrigeredEvent;
import com.m2msolutions.m2mcomputadordebordo.util.AppHelper;

public class EventoRowHolderInfo {
	
	public ImageView imageView;
	public TextView  textView;
	public TextView  eventoHora;
	
	public void drawRow(TrigeredEvent obj,Context context){
		if(obj.getEvento()!= null && obj.getEvento().getAlertaTipo()!= null){
			switch (obj.getEvento().getAlertaTipo()) {
			case AUDIO:
					imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icone_mensagem_audio));
				break;
				
			case TEXTO:
				imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icone_mensagem_escrita));
				
				break;
			default:
				break;

			}
			
			if(obj.getEvento()!= null  && obj.getEvento().getTexto()!= null){
				textView.setText(obj.getEvento().getTexto());
			}else{
				textView.setText("");				
			}
			
			if(obj.getDate()!= null){
				eventoHora.setText(AppHelper.getInstance().formatTime(obj.getDate()));
			}else{
				eventoHora.setText("");				
			}
		}
	}

}
