package com.m2msolutions.m2mcomputadordebordo.adapter.holderinfo;

import android.app.Activity;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.m2msolutions.m2mcomputadordebordo.R;
import com.m2msolutions.m2mcomputadordebordo.model.Message;
import com.m2msolutions.m2mcomputadordebordo.util.AppHelper;

public class MessageRowHolderInfo {
	
	public TextView messageBodyText;
	public TextView messageDateText;
	public TextView messageOwnnerIcon;
	public LinearLayout messsageBackground;
	
	public void drawRow(Message obj, Activity context){
		if(obj.getMessage() != null){
			messageBodyText.setText(obj.getMessage());
		}else{
			messageBodyText.setText("");
			
		}
		
		if(obj.getMessageOwner() != null){
			switch (obj.getMessageOwner()) {
			case MOTORISTA:
				messageOwnnerIcon.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.school_bus));
				break;
				
			case OPERDADOR:
				messageOwnnerIcon.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.user_1));				
				break;

			default:
				break;
			}
			
		}
		
		if(obj.getDate() !=  null){
			messageDateText.setText(AppHelper.getInstance().formatTime( obj.getDate().getTime()));
		}else{
			messageDateText.setText("");
		}
		
	}

}
