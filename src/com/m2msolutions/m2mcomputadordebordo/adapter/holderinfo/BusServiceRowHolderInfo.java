package com.m2msolutions.m2mcomputadordebordo.adapter.holderinfo;

import android.widget.TextView;

import com.m2msolutions.m2mcomputadordebordo.model.Rota;

public class BusServiceRowHolderInfo {

	public TextView busServiceNameField;

	public void drawRow(Rota rota) {
		if (rota == null) {
			return;
		}

		if(rota.getNome() != null)
			busServiceNameField.setText(rota.getNome());
		else
			busServiceNameField.setText("");
	}

}
