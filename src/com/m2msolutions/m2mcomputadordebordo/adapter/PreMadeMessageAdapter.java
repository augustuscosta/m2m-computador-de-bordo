package com.m2msolutions.m2mcomputadordebordo.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.m2msolutions.m2mcomputadordebordo.R;
import com.m2msolutions.m2mcomputadordebordo.adapter.holderinfo.MensagemPreRowHolderInfo;
import com.m2msolutions.m2mcomputadordebordo.model.MensagemPre;

public class PreMadeMessageAdapter extends BaseAdapter {

	private List<MensagemPre> messages;
	private Activity context;
	
	public PreMadeMessageAdapter(Activity context, List<MensagemPre> messages) {
		this.context = context;
		this.messages = messages;
	}
	
	@Override
	public int getCount() {
		return messages == null ? 0 : messages.size();
	}

	@Override
	public Object getItem(int position) {
		return messages == null ? 0 : messages.get(position);
	}

	@Override
	public long getItemId(int position) {
		return messages == null ? 0 : position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		View row = view;
		MensagemPreRowHolderInfo rowInfo ;
		
		if (view == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			row = inflater.inflate(R.layout.message_pre_render_row, null);
			
			rowInfo = new MensagemPreRowHolderInfo();
			rowInfo.message = (TextView) row.findViewById(R.id.message);
			
			row.setTag(rowInfo);
		} else {
			rowInfo = (MensagemPreRowHolderInfo) view.getTag();	
		}
		
		String message = messages.get(position).getMessage();
		rowInfo.drawRow(message);
		return row;
	}

}
