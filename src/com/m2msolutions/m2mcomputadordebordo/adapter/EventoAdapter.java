package com.m2msolutions.m2mcomputadordebordo.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.m2msolutions.m2mcomputadordebordo.R;
import com.m2msolutions.m2mcomputadordebordo.adapter.holderinfo.EventoRowHolderInfo;
import com.m2msolutions.m2mcomputadordebordo.model.TrigeredEvent;

public class EventoAdapter extends BaseAdapter {

	private List<TrigeredEvent> eventos;
	private Activity context;
	
	public EventoAdapter(Activity context, List<TrigeredEvent> eventos) {
		this.context = context;
		this.eventos = eventos;
	}
	
	@Override
	public int getCount() {
		return eventos == null ? 0 : eventos.size();
	}

	@Override
	public Object getItem(int position) {
		return eventos == null ? 0 : eventos.get(position);
	}

	@Override
	public long getItemId(int position) {
		return eventos == null ? 0 : position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		View row = view;
		EventoRowHolderInfo rowInfo ;
		
		if (view == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			row = inflater.inflate(R.layout.evento_row_holder, null);
			
			rowInfo = new EventoRowHolderInfo();
			rowInfo.textView = (TextView) row.findViewById(R.id.eventoDescription);
			rowInfo.eventoHora = (TextView) row.findViewById(R.id.eventoHora);
			rowInfo.imageView = (ImageView) row.findViewById(R.id.eventoTypeImage);
			
			row.setTag(rowInfo);
		} else {
			rowInfo = (EventoRowHolderInfo) view.getTag();	
		}
		
		TrigeredEvent evento = eventos.get(position);
		rowInfo.drawRow(evento,context);
		return row;
	}

}
