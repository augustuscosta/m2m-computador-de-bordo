package com.m2msolutions.m2mcomputadordebordo.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.m2msolutions.m2mcomputadordebordo.R;
import com.m2msolutions.m2mcomputadordebordo.adapter.holderinfo.MessageRowHolderInfo;
import com.m2msolutions.m2mcomputadordebordo.model.Message;

public class MessageAdapter extends BaseAdapter{
	
	private List<Message> mensagens;
	private Activity context;
	
	
	public MessageAdapter(List<Message> mensagens,  Activity context){
		this.context =context;
		this.mensagens = mensagens;
	}
	
	@Override
	public int getCount() {
		return mensagens == null ?0 : mensagens.size();
	}
	
	@Override
	public Message getItem(int position) {
		 return mensagens == null ? null : mensagens.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		return position;
	}
	
	@Override
	public View getView(int position, View view, ViewGroup parent) {
		View row = view;
		MessageRowHolderInfo rowInfo;
		Message obj = mensagens.get(position);
		if(row == null){
			LayoutInflater inflater = context.getLayoutInflater();
			row = inflater.inflate(R.layout.message_row_holder_info,null);

			rowInfo = new MessageRowHolderInfo();
			rowInfo.messageBodyText = (TextView) row.findViewById(R.id.message);
			rowInfo.messageDateText = (TextView) row.findViewById(R.id.messageDates);
			rowInfo.messageOwnnerIcon = (TextView) row.findViewById(R.id.messageOwner);
			rowInfo.messsageBackground = (LinearLayout) row.findViewById(R.id.messageBackground);
			
			row.setTag(rowInfo);
		}else{
			rowInfo = (MessageRowHolderInfo) view.getTag();	
		}
		
		rowInfo.drawRow(obj, context);
		return row;
	}

}
