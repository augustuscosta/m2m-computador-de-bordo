package com.m2msolutions.m2mcomputadordebordo.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.m2msolutions.m2mcomputadordebordo.R;
import com.m2msolutions.m2mcomputadordebordo.adapter.holderinfo.BusServiceRowHolderInfo;
import com.m2msolutions.m2mcomputadordebordo.model.Rota;

public class BusServiceAdapter extends BaseAdapter {

	private List<Rota> rotas;
	private Activity context;
	
	public BusServiceAdapter(Activity context, List<Rota> rotas) {
		this.context = context;
		this.rotas = rotas;
	}
	
	@Override
	public int getCount() {
		return rotas == null ? 0 : rotas.size();
	}

	@Override
	public Object getItem(int position) {
		return rotas == null ? 0 : rotas.get(position);
	}

	@Override
	public long getItemId(int position) {
		return rotas == null ? 0 : position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		View row = view;
		BusServiceRowHolderInfo rowInfo ;
		
		if (view == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			row = inflater.inflate(R.layout.bus_service_render_row, null);
			
			rowInfo = new BusServiceRowHolderInfo();
			rowInfo.busServiceNameField = (TextView) row.findViewById(R.id.busServiceName);
			
			row.setTag(rowInfo);
		} else {
			rowInfo = (BusServiceRowHolderInfo) view.getTag();	
		}
		
		Rota busService = rotas.get(position);
		rowInfo.drawRow(busService);
		return row;
	}

}
