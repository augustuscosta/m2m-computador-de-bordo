package com.m2msolutions.m2mcomputadordebordo.sdcardfiles.soundsandpics;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Environment;

import com.m2msolutions.m2mcomputadordebordo.database.EventoDAO;
import com.m2msolutions.m2mcomputadordebordo.database.MotoristaDAO;
import com.m2msolutions.m2mcomputadordebordo.model.AlertaTipo;
import com.m2msolutions.m2mcomputadordebordo.model.Evento;
import com.m2msolutions.m2mcomputadordebordo.model.Motorista;
import com.m2msolutions.m2mcomputadordebordo.sdcardfiles.FolderConfigurations;
import com.m2msolutions.m2mcomputadordebordo.util.ConstUtil;
import com.m2msolutions.m2mcomputadordebordo.util.LogUtil;
import com.m2msolutions.m2mcomputadordebordo.util.Session;

public class SoundAndPicturesTransfer {

	private static final String LOGO_PNG = "logo.png";

	public static void copyDirectory(File sourceLocation , Context context) throws IOException {
		if(!sourceLocation.exists()) return;
		if(sourceLocation.isDirectory()){
			String[] children = sourceLocation.list();
			for (int i = 0; i < children.length; i++) {
				File file = new File(children[i]);
				copyFile(sourceLocation, file, context);
			}
		}else{
				copyFile(sourceLocation, null, context);
		}
	}

	private static void copyFile(File sourceLocation, File children,Context context)
			throws FileNotFoundException, IOException {
		InputStream in;
		OutputStream out;
		byte[] buf;
		if(children != null){
		 in = new FileInputStream(sourceLocation+"/"+children);
		 out = context.openFileOutput(children.getName(), Context.MODE_PRIVATE);
		 buf = new byte[(int) in.available()];
		}else{
			in = new FileInputStream(sourceLocation);			
		out = context.openFileOutput(sourceLocation.getName(), Context.MODE_PRIVATE);
		buf = new byte[(int) in.available()];
		}
		int len;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
		in.close();
		out.close();
	}

	public static void copyPicsToContext(Context context) throws IOException{
		File source = new File(Environment.getExternalStorageDirectory(),FolderConfigurations.MOTORISTA_PIC_FOLDER);
		copyDirectory(source, context);
	}

	public static void copySoundsToContext(Context context) throws IOException{
		List<Evento> eventos = new EventoDAO(context).getAll();
		for (Evento evento : eventos) {
			if(evento.getAlertaTipo() == AlertaTipo.AUDIO){
				copySoundFilesFromEvents( new File(Environment.getExternalStorageDirectory()+"/"+evento.getMp3Path()), context, evento.getId()+".mp3");				
			}
		}
	}

	public static void copyAll(Context context) throws Exception{
		copyPicsToContext(context);
		copySoundsToContext(context);
	}
	
	@SuppressLint("WorldReadableFiles")
	private static void copySoundFilesFromEvents(final File sourceLocation,Context context, String newFileName){
		InputStream in;
		OutputStream out;
		byte[] buf;
		try {
			in = new FileInputStream(sourceLocation);
			out = context.openFileOutput(newFileName, Context.MODE_WORLD_READABLE);
			buf = new byte[(int) in.available()];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			in.close();
			out.close();
		} catch (Exception e) {
			LogUtil.e("erro ao copiar som do evento", e);
		}			
	}
	
	public static void copyCompanyLogo(Context context){
		InputStream in;
		OutputStream out;
		byte[] buf;
		try {
			in = new FileInputStream(Environment.getExternalStorageDirectory()+FolderConfigurations.LOGO_PATH);
			out = context.openFileOutput(LOGO_PNG, Context.MODE_PRIVATE);
			buf = new byte[(int) in.available()];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			in.close();
			out.close();
		} catch (Exception e) {
			LogUtil.e("erro ao copiar som do evento", e);
		}
	}
	
	
	public static BitmapDrawable loadLogo(Context context) {
		FileInputStream fis;
		try {
			fis = context.openFileInput(LOGO_PNG);
			Bitmap bitmap = BitmapFactory.decodeStream(fis);
			BitmapDrawable bitmapDrawable = new BitmapDrawable(bitmap);
			return bitmapDrawable;
		} catch (Exception e) {
			LogUtil.e("erro ao carregar foto do motorista", e);
		}
		return null;
	}

	public static BitmapDrawable loadMotoristaPic(Context context) {
		MotoristaDAO dao = new MotoristaDAO(context);
		Motorista motorista = dao.fing(Session.getDriverId(context));
		if(motorista == null) return null;

		FileInputStream fis;
		try {
			fis = context.openFileInput(motorista.getMatricula()+motorista.getNome()+ConstUtil.PNG);
			Bitmap bitmap = BitmapFactory.decodeStream(fis);
			BitmapDrawable bitmapDrawable = new BitmapDrawable(bitmap);
			return bitmapDrawable;
		} catch (Exception e) {
			LogUtil.e("erro ao carregar foto do motorista", e);
		}
		return null;
	}

}
