package com.m2msolutions.m2mcomputadordebordo.sdcardfiles.soundsandpics.test;

import com.m2msolutions.m2mcomputadordebordo.sdcardfiles.soundsandpics.SoundAndPicturesTransfer;
import com.m2msolutions.m2mcomputadordebordo.util.LogUtil;
import com.m2msolutions.m2mcomputadordebordo.util.Session;

import android.graphics.drawable.BitmapDrawable;
import android.test.AndroidTestCase;

public class FileTransferTests extends AndroidTestCase {
	
	@Override
	protected void setUp() throws Exception {
		LogUtil.initLog(getContext(), "testCB");
		Session.setDriverId((long)1, getContext());
		super.setUp();
	}
	
	public void testLoadPic() throws Exception{
		BitmapDrawable bitmapDrawable = SoundAndPicturesTransfer.loadMotoristaPic(getContext());
		assertNotNull(bitmapDrawable);
	}

}
