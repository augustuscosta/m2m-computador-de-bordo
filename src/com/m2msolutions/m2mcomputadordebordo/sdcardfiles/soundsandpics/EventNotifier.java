package com.m2msolutions.m2mcomputadordebordo.sdcardfiles.soundsandpics;

import android.content.Context;
import android.media.MediaPlayer;

import com.m2msolutions.m2mcomputadordebordo.model.TrigeredEvent;
import com.m2msolutions.m2mcomputadordebordo.util.AppHelper;
import com.m2msolutions.m2mcomputadordebordo.util.LogUtil;

public class EventNotifier {

	public static void playbackNoitifications(TrigeredEvent evento,Context context){
		if(evento == null || evento.getEvento() == null){
			return;
		}

		MediaPlayer mp = new MediaPlayer();
		try {
			mp.setDataSource(context.getFileStreamPath(evento.getEvento().getId()+".mp3").getPath());
			mp.prepare();
			mp.setLooping(false);
			mp.start();
		} catch (Exception e) {
			LogUtil.e("erro ao reproduzir Mp3 do evento", e);
		}
	}


	public static void showNotifications(TrigeredEvent evento,Context context){
		if(evento == null || evento.getEvento() == null){
			return;
		}

		AppHelper.presentNewEvento(context, evento);
	}



}
