package com.m2msolutions.m2mcomputadordebordo.sdcardfiles;

public class FolderConfigurations {
	
	public static final String MOTORISTA_XML_FILE = "/m2mcomputadordebordo/motoristas.xml";
	
	public static final String PONTOS_XML_FILE = "/m2mcomputadordebordo/pontos.xml";
	
	public static final String ROTA_XML_FILE ="/m2mcomputadordebordo/rotas.xml";
	
	public static final String EVENTOS_XML_FILE = "/m2mcomputadordebordo/eventos.xml";
	
	public static final String CONFIGURACOES_XML_FILE = "/m2mcomputadordebordo/configuracao.xml";
	
	public static final String MENSAGENS_PRE_XML_FILE = "/m2mcomputadordebordo/mensagenspre.xml";
	
	public static final String EVENT_SOUND_FOLDER ="/m2mcomputadordebordo/evento/eventoMp3";
	
	public static final String MOTORISTA_PIC_FOLDER ="/m2mcomputadordebordo/motoristas/fotos/";
	
	public static final String LOGO_PATH ="/m2mcomputadordebordo/logo/logo.png";

}
