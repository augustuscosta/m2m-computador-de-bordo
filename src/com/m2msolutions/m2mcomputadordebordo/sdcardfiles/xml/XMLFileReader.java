package com.m2msolutions.m2mcomputadordebordo.sdcardfiles.xml;

import java.io.File;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import android.content.Context;
import android.os.Environment;

import com.m2msolutions.m2mcomputadordebordo.database.ConfiguracaoDAO;
import com.m2msolutions.m2mcomputadordebordo.database.EventoDAO;
import com.m2msolutions.m2mcomputadordebordo.database.MensagemPreDao;
import com.m2msolutions.m2mcomputadordebordo.database.MotoristaDAO;
import com.m2msolutions.m2mcomputadordebordo.database.PontoDao;
import com.m2msolutions.m2mcomputadordebordo.database.RotaDAO;
import com.m2msolutions.m2mcomputadordebordo.model.xmlrootobjects.Configuracoes;
import com.m2msolutions.m2mcomputadordebordo.model.xmlrootobjects.Eventos;
import com.m2msolutions.m2mcomputadordebordo.model.xmlrootobjects.MensagensPre;
import com.m2msolutions.m2mcomputadordebordo.model.xmlrootobjects.Motoristas;
import com.m2msolutions.m2mcomputadordebordo.model.xmlrootobjects.Pontos;
import com.m2msolutions.m2mcomputadordebordo.model.xmlrootobjects.Rotas;
import com.m2msolutions.m2mcomputadordebordo.sdcardfiles.FolderConfigurations;
import com.m2msolutions.m2mcomputadordebordo.sdcardfiles.soundsandpics.SoundAndPicturesTransfer;
import com.m2msolutions.m2mcomputadordebordo.util.LogUtil;
import com.m2msolutions.m2mcomputadordebordo.util.Session;

public class XMLFileReader {
	
	
	public static Rotas getRotasFromFile(){
		File dir = Environment.getExternalStorageDirectory();		
		Serializer serial = new Persister();
		File file = new File(dir,FolderConfigurations.ROTA_XML_FILE);
		Rotas rotas = null;
		try {
			 rotas = serial.read(Rotas.class, file);
			LogUtil.i( "Rotas disponiveis: " + rotas.listaRotas.size());
		} catch (Exception e) {
			LogUtil.e("erro ao pegar as listaRotas", e);
		}
		
		return rotas;
	}
	
	public static Motoristas getMotoristasFromFile(){
		File dir = Environment.getExternalStorageDirectory();		
		Serializer serial = new Persister();
		File file = new File(dir,FolderConfigurations.MOTORISTA_XML_FILE);
		Motoristas motoristas = null;
		try {
			motoristas = serial.read(Motoristas.class, file);
			LogUtil.i( "Motoristas disponiveis: " + motoristas.listaMotoristas.size());
		} catch (Exception e) {
			LogUtil.e("erro ao pegar os  listaMotoristas", e);
		}
		
		return motoristas;
	}
	
	public static Pontos getPontosFromFile(){
		File dir = Environment.getExternalStorageDirectory();		
		Serializer serial = new Persister();
		File file = new File(dir,FolderConfigurations.PONTOS_XML_FILE);
		Pontos pontos = null;
		try {
			pontos = serial.read(Pontos.class, file);
			LogUtil.i( "Pontos disponiveis: " + pontos.listaPontos.size());
		} catch (Exception e) {
			LogUtil.e("erro ao pegar os listaPontos", e);
		}
		
		return pontos;
	}
	
	public static Configuracoes getConfiguracoesFromFile(){
		File dir = Environment.getExternalStorageDirectory();		
		Serializer serial = new Persister();
		File file = new File(dir,FolderConfigurations.CONFIGURACOES_XML_FILE);
		Configuracoes configuracoes = null;
		try {
			configuracoes = serial.read(Configuracoes.class, file);
			LogUtil.i( "Configurações disponiveis: " + configuracoes.listaConfiguracoes.size());
		} catch (Exception e) {
			LogUtil.e("erro ao pegar as configuracoes", e);
		}
		
		return configuracoes;
	}
	
	public static MensagensPre getMensagensPreFromFile(){
		File dir = Environment.getExternalStorageDirectory();		
		Serializer serial = new Persister();
		File file = new File(dir,FolderConfigurations.MENSAGENS_PRE_XML_FILE);
		MensagensPre mensagensPre = null;
		try {
			mensagensPre = serial.read(MensagensPre.class, file);
			LogUtil.i( "Mensagens pré definidas disponiveis: " + mensagensPre.listaMensagensPre.size());
		} catch (Exception e) {
			LogUtil.e("erro ao pegar as Mensagens pre definidas", e);
		}
		
		return mensagensPre;
	}
	
	public static Eventos getEventosFromFile(){
		File dir = Environment.getExternalStorageDirectory();		
		Serializer serial = new Persister();
		File file = new File(dir,FolderConfigurations.EVENTOS_XML_FILE);
		Eventos eventos = null;
		try {
			eventos = serial.read(Eventos.class, file);
			LogUtil.i( "Eventos disponiveis: " + eventos.listaEventos.size());
		} catch (Exception e) {
			LogUtil.e("erro ao pegar as Mensagens pre definidas", e);
		}
		
		return eventos;
	}

	
	public static void readAndSaveAll(Context context){
		persistConfigurações(context);
		Session.setConifgFromDb(context);
		persistEventos(context);
		persistMensagensPré(context);
		persistMotoristas(context);
		persistPontos(context);
		persistRota(context);
		SoundAndPicturesTransfer.copyCompanyLogo(context);
	}
	
	public static void persistConfigurações(Context context) {
		ConfiguracaoDAO cPDao = new ConfiguracaoDAO(context);
		Configuracoes configuracoes = XMLFileReader.getConfiguracoesFromFile();
		if(configuracoes != null){
			cPDao.persist(configuracoes.listaConfiguracoes);			
		}
	}

	public static void persistMensagensPré(Context context) {
		MensagemPreDao mPDao = new MensagemPreDao(context);
		MensagensPre mensagensPre = XMLFileReader.getMensagensPreFromFile();
		if(mensagensPre != null){
			mPDao.persist(mensagensPre.listaMensagensPre);
		}
	}

	public static void persistRota(Context context) {
		RotaDAO rDao = new RotaDAO(context);
		Rotas rotas = XMLFileReader.getRotasFromFile();
		if(rotas!= null){
			rDao.persist(rotas.listaRotas);			
		}
	}

	public static void persistPontos(Context context) {
		PontoDao pDao = new PontoDao(context);
		Pontos pontos = XMLFileReader.getPontosFromFile();
		if(pontos != null){
			pDao.persist(pontos.listaPontos);			
		}
	}

	public static void persistEventos(Context context) {
		EventoDAO eDao = new EventoDAO(context);
		Eventos eventos =XMLFileReader.getEventosFromFile();
		if(eventos!= null){
			eDao.persist(eventos.listaEventos);			
		}
	}

	public static void persistMotoristas(Context context) {
		MotoristaDAO mDao = new MotoristaDAO(context);
		Motoristas motoristas = XMLFileReader.getMotoristasFromFile();
		if(motoristas != null){
			mDao.persist(motoristas.listaMotoristas);			
		}
	}
}
