package com.m2msolutions.m2mcomputadordebordo.sdcardfiles.xml.test;

import java.io.File;
import java.util.Date;
import java.util.List;

import junit.framework.Assert;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import android.os.Environment;
import android.test.AndroidTestCase;

import com.m2msolutions.m2mcomputadordebordo.database.ConfiguracaoDAO;
import com.m2msolutions.m2mcomputadordebordo.database.EventoDAO;
import com.m2msolutions.m2mcomputadordebordo.database.MensagemPreDao;
import com.m2msolutions.m2mcomputadordebordo.database.MotoristaDAO;
import com.m2msolutions.m2mcomputadordebordo.database.PontoDao;
import com.m2msolutions.m2mcomputadordebordo.database.RotaDAO;
import com.m2msolutions.m2mcomputadordebordo.database.TrigeredEventDao;
import com.m2msolutions.m2mcomputadordebordo.model.AlertaTipo;
import com.m2msolutions.m2mcomputadordebordo.model.Configuracao;
import com.m2msolutions.m2mcomputadordebordo.model.Evento;
import com.m2msolutions.m2mcomputadordebordo.model.MensagemPre;
import com.m2msolutions.m2mcomputadordebordo.model.Motorista;
import com.m2msolutions.m2mcomputadordebordo.model.NivelTemperatura;
import com.m2msolutions.m2mcomputadordebordo.model.Ponto;
import com.m2msolutions.m2mcomputadordebordo.model.Rota;
import com.m2msolutions.m2mcomputadordebordo.model.TrigeredEvent;
import com.m2msolutions.m2mcomputadordebordo.model.xmlrootobjects.Configuracoes;
import com.m2msolutions.m2mcomputadordebordo.model.xmlrootobjects.Eventos;
import com.m2msolutions.m2mcomputadordebordo.model.xmlrootobjects.MensagensPre;
import com.m2msolutions.m2mcomputadordebordo.model.xmlrootobjects.Motoristas;
import com.m2msolutions.m2mcomputadordebordo.model.xmlrootobjects.Pontos;
import com.m2msolutions.m2mcomputadordebordo.model.xmlrootobjects.Rotas;
import com.m2msolutions.m2mcomputadordebordo.sdcardfiles.FolderConfigurations;
import com.m2msolutions.m2mcomputadordebordo.sdcardfiles.xml.XMLFileReader;
import com.m2msolutions.m2mcomputadordebordo.util.AppHelper;
import com.m2msolutions.m2mcomputadordebordo.util.LogUtil;

public class CoreXMLWriteReadAndPersistTest extends AndroidTestCase{

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		LogUtil.initLog(getContext(), "testLog");
	}

	public void testWriteRotasXML() throws Exception {
		Rotas rotas = new Rotas();
		Rota rota = new Rota();
		rota.setId((long)1);
		rota.setNome("Siqueira/Papicu");
		rota.setTotalKm(7808.9);
		rota.setTotalTempo((long)7298373);
		rotas.listaRotas.add(rota);
		Rota rota2 = new Rota();
		rota2.setId((long)2);
		rota2.setNome("rotaTeste");
		rota2.setTotalKm(7808.9);
		rota2.setTotalTempo((long)7298373);
		rotas.listaRotas.add(rota2);
		LogUtil.i("Escrevendo rota para o arquivo");
		File dir = Environment.getExternalStorageDirectory();		
		Serializer serial = new Persister();
		File file = new File(dir,FolderConfigurations.ROTA_XML_FILE);
		serial.write(rotas, file);
		Rotas rotas2 = serial.read(Rotas.class, file);
		Assert.assertNotNull(rotas2);
		Assert.assertTrue(rotas2.listaRotas.size() == 2);
		Assert.assertTrue(rotas2.listaRotas.get(0).getNome().equals(rota.getNome()));
		writePontosXML();
		Pontos pontos = XMLFileReader.getPontosFromFile();
		PontoDao pontoDao = new PontoDao(getContext());
		pontoDao.persist(pontos.listaPontos);
		RotaDAO rotaDAO = new RotaDAO(getContext());
		rotaDAO.persist(rotas2.listaRotas);
		List<Rota> rotasFromDb = rotaDAO.getRotasList();
		Assert.assertNotNull(rotasFromDb);
		Assert.assertTrue(!rotasFromDb.isEmpty());
		Assert.assertTrue(rotasFromDb.size() == 2);
		Assert.assertTrue(rotasFromDb.get(0).getPontos().size() == 3);

	}

	public void testWriteMotoristaXMl() throws Exception{
		File dir = Environment.getExternalStorageDirectory();		
		Motoristas motoristas = new Motoristas();
		Motorista motorista = new Motorista();
		motorista.setNome("Valdemiro");
		motorista.setId((long) 1);
		motorista.setRfId("89078908098080800809");
		motorista.setSenha("admin");
		motorista.setMatricula("matriculaMotoristaTeste");
		Motorista motorista2 = new Motorista();
		motorista2.setNome("Roger");
		motorista2.setId((long) 2);
		motorista2.setRfId("89072308098080800809");
		motorista2.setSenha("admin2");
		motorista2.setMatricula("admin234");
		motoristas.listaMotoristas.add(motorista);
		motoristas.listaMotoristas.add(motorista2);
		LogUtil.i("Escrevendo motorista para o arquivo");
		Serializer serial = new Persister();
		File file = new File(dir,FolderConfigurations.MOTORISTA_XML_FILE);
		serial.write(motoristas, file);
		Motoristas motoritas2 = XMLFileReader.getMotoristasFromFile();
		Assert.assertNotNull(motoritas2);
		Assert.assertTrue(motoritas2.listaMotoristas.size() == 2);
		Assert.assertTrue(motoritas2.listaMotoristas.get(0).getNome().equals(motorista.getNome()));
		MotoristaDAO motoristaDAO = new MotoristaDAO(getContext());
		motoristaDAO.persist(motoritas2.listaMotoristas);
		List<Motorista> motoristas3 = motoristaDAO.listAll();
		Assert.assertNotNull(motoristas3);
		Assert.assertFalse(motoristas3.isEmpty());
		Assert.assertTrue(motoristas3.get(0).getNome().equals(motorista.getNome()));
	}

	public void testWriteEventoXMl() throws Exception{
		File dir = Environment.getExternalStorageDirectory();		
		Eventos eventos = new Eventos();
		Evento evento = new Evento();
		evento.setAlertaTipo(AlertaTipo.AUDIO);
		evento.setId((long) 1);
		evento.setMp3Path("m2mcomputadordebordo/eventoMp3/evento.mp3");
		evento.setTexto("texto de teste");
		eventos.listaEventos.add(evento);
		Evento evento2 = new Evento();
		evento2.setAlertaTipo(AlertaTipo.TEXTO);
		evento2.setId((long) 2);
		evento2.setMp3Path("sem arquivo");
		evento2.setTexto("texto de teste");
		eventos.listaEventos.add(evento2);
		LogUtil.i("Escrevendo rota para o arquivo");
		Serializer serial = new Persister();
		File file = new File(dir,FolderConfigurations.EVENTOS_XML_FILE);
		serial.write(eventos, file);
		Eventos eventos1 = XMLFileReader.getEventosFromFile();
		Assert.assertNotNull(eventos1);
		Assert.assertTrue(eventos1.listaEventos.size() == 2);
		Assert.assertTrue(eventos1.listaEventos.get(0).getMp3Path().equals(evento.getMp3Path()));
		EventoDAO eventoDAO = new EventoDAO(getContext());
		eventoDAO.persist(eventos1.listaEventos);
		List<Evento> eventos3 = eventoDAO.getAll();
		Assert.assertNotNull(eventos3);
		Assert.assertFalse(eventos3.isEmpty());
		Assert.assertTrue(eventos3.get(0).getTexto().equals(evento.getTexto()));
		TrigeredEvent eventoAcionado = new TrigeredEvent();
		eventoAcionado.setDate(new Date(AppHelper.getCurrentTime()));
		eventoAcionado.setEvento(eventoDAO.find(1));
		eventoAcionado.setLatitude(-33.4);
		eventoAcionado.setLongitude(-44.5);
		eventoAcionado.setEventoId((long)1);
		TrigeredEvent eventoAcionado2 = new TrigeredEvent();
		eventoAcionado2.setDate(new Date(AppHelper.getCurrentTime()));
		eventoAcionado2.setEvento(eventoDAO.find(2));
		eventoAcionado2.setLatitude(-33.4);
		eventoAcionado2.setLongitude(-44.5);
		eventoAcionado2.setEventoId((long)2);
		TrigeredEventDao tDao = new TrigeredEventDao(getContext());
		tDao.persist(eventoAcionado);
		tDao.persist(eventoAcionado2);
	}  

	public void testWriteMensagemPreXMl() throws Exception{
		File dir = Environment.getExternalStorageDirectory();
		MensagensPre mensagensPre = new MensagensPre();
		MensagemPre mensagemPre = new MensagemPre();
		mensagemPre.setId((long) 0);
		mensagemPre.setMessage("Pânico");
		mensagensPre.listaMensagensPre.add(mensagemPre);
		MensagemPre mensagemPre1 = new MensagemPre();
		mensagemPre1.setId((long) 1);
		mensagemPre1.setMessage("Assalto");
		mensagensPre.listaMensagensPre.add(mensagemPre1);
		MensagemPre mensagemPre2 = new MensagemPre();
		mensagemPre2.setId((long) 2);
		mensagemPre2.setMessage("Sequestro de veículo");
		mensagensPre.listaMensagensPre.add(mensagemPre2);
		MensagemPre mensagemPre3 = new MensagemPre();
		mensagemPre3.setId((long) 3);
		mensagemPre3.setMessage("Sem GPS");
		mensagensPre.listaMensagensPre.add(mensagemPre3);
		LogUtil.i("Escrevendo rota para o arquivo");
		Serializer serial = new Persister();
		File file = new File(dir,FolderConfigurations.MENSAGENS_PRE_XML_FILE);
		try {
			serial.write(mensagensPre, file);
		} catch (Exception e) {
			LogUtil.e("erro ao ler Mensagens Pré definidas do novo arquivo", e);
			throw e;
		}
		MensagensPre mensagensPre2 = XMLFileReader.getMensagensPreFromFile();
		Assert.assertNotNull(mensagensPre2);
		Assert.assertTrue(mensagensPre2.listaMensagensPre.size() == 4);
		Assert.assertTrue(mensagensPre2.listaMensagensPre.get(0).getMessage().equals(mensagemPre.getMessage()));
		MensagemPreDao mensagemPreDao = new MensagemPreDao(getContext());
		mensagemPreDao.persist(mensagensPre2.listaMensagensPre);
		List<MensagemPre> mensagemPres = mensagemPreDao.getAllFromDataBase();
		Assert.assertNotNull(mensagemPres);
		Assert.assertFalse(mensagemPres.isEmpty());
		Assert.assertTrue(mensagemPres.get(0).getMessage().equals(mensagemPre.getMessage()));
		
	}
	
	public void testWriteConfigs()throws Exception{
		File dir = Environment.getExternalStorageDirectory();
		Configuracoes configuracoes = new Configuracoes();
		Configuracao configuracao = new Configuracao();
		configuracao.setMasterPassword("00001");
		configuracao.setNivelTemperatura(NivelTemperatura.MEDIA);
		configuracao.setVelocidade(true);
		configuracao.setVelocidadeMaxima((double)200);
		configuracao.setId((long) 1);
		configuracao.setConfigTime((long) 180000);
		configuracoes.listaConfiguracoes.add(configuracao);
		Serializer serial = new Persister();
		File file = new File(dir,FolderConfigurations.CONFIGURACOES_XML_FILE);
		try {
			serial.write(configuracoes, file);
		} catch (Exception e) {
			LogUtil.e("erro ao ler Mensagens Pré definidas do novo arquivo", e);
			throw e;
		}
		Configuracoes configuracoes2 = XMLFileReader.getConfiguracoesFromFile();
		Assert.assertNotNull(configuracoes2);
		Assert.assertTrue(configuracoes2.listaConfiguracoes.size() == 1);
		Assert.assertTrue(configuracoes2.listaConfiguracoes.get(0).getMasterPassword().equals(configuracao.getMasterPassword()));
		ConfiguracaoDAO dao = new ConfiguracaoDAO(getContext());
		dao.persist(configuracoes2.listaConfiguracoes);
		Configuracao configuracao2 = dao.get();
		Assert.assertNotNull(configuracao2);
		Assert.assertTrue(configuracao2.getMasterPassword().equals(configuracao.getMasterPassword()));
	}

	public void writePontosXML() throws Exception{
		File dir = Environment.getExternalStorageDirectory();
		Pontos pontos = new Pontos();
		Ponto ponto = new Ponto();
		ponto.setId((long)1);
		ponto.setFraction((double)0);
		ponto.setIdRota((long)1);
		ponto.setIndice((long)1);
		ponto.setNome("Terminal da Siqueira");
		ponto.setTempoMedio((long) 0);
		ponto.setLatitude(-34.5);
		ponto.setLongitude(-44.3);
		Ponto ponto2 = new Ponto();
		ponto2.setId((long)2);
		ponto2.setFraction((double)0.5);
		ponto2.setIdRota((long)1);
		ponto2.setIndice((long)2);
		ponto2.setNome("Dom Luis");
		ponto2.setTempoMedio((long) 3649185);
		ponto2.setLatitude(-34.6);
		ponto2.setLongitude(-44.7);
		Ponto ponto3 = new Ponto();
		ponto3.setId((long)3);
		ponto3.setFraction((double) 0.5);
		ponto3.setIdRota((long)1);
		ponto3.setIndice((long)3);
		ponto3.setNome("Terminal Papicu");
		ponto3.setTempoMedio((long) 3649187);
		ponto3.setLatitude(-34.6);
		ponto3.setLongitude(-44.7);
		pontos.listaPontos.add(ponto);
		pontos.listaPontos.add(ponto2);
		pontos.listaPontos.add(ponto3);
		File file = new File(dir,FolderConfigurations.PONTOS_XML_FILE);
		Serializer serial = new Persister();
		try {
			serial.write(pontos, file);
		}catch (Exception e) {
			LogUtil.e("erro ao escrever pontos para o novo arquivo", e);
			throw e;
		}
		Pontos pontos2 = XMLFileReader.getPontosFromFile();
		Assert.assertNotNull(pontos2);
		Assert.assertTrue(pontos2.listaPontos.size() == 3);
		Assert.assertTrue(pontos.listaPontos.get(0).getNome().equals(ponto.getNome()));
		Assert.assertTrue(pontos.listaPontos.get(1).getNome().equals(ponto2.getNome()));
	}

}
