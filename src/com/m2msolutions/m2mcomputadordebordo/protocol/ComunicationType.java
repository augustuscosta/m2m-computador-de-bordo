package com.m2msolutions.m2mcomputadordebordo.protocol;

public enum ComunicationType {
	
	
	CHAT("01"),
	VEHICLESTATUS("");
	
	private String value;
	
	ComunicationType(String value){
		this.value = value;
	}
	
	public static ComunicationType fromValue(String value){
		for (ComunicationType comunicationType : ComunicationType.values()) {
			if(comunicationType.getValue().equals(value)) return comunicationType;
		}
		
		return null;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
