package com.m2msolutions.m2mcomputadordebordo.protocol;


public enum ProtocolType {
	
	CHAT("3C"),
	UNDEFINED("1F"),
	VEHICLESTATUS("00");

	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	private ProtocolType(String value) {
		this.value = value;
	}
	
	
	public static ProtocolType fromValue(String value){
		for (ProtocolType protocolType : ProtocolType.values()) {
			if(value.equalsIgnoreCase(value)) return protocolType;
		}
		return null;
	}
	
	
}
