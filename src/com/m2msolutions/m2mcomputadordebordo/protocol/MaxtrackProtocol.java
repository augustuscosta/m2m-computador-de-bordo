package com.m2msolutions.m2mcomputadordebordo.protocol;

import java.io.UnsupportedEncodingException;

import com.m2msolutions.m2mcomputadordebordo.util.LogUtil;

public class MaxtrackProtocol {

	final String specialCharacter = "01";
	final String specialCharacter2 = "04";
	final String specialCharacter3 = "10";
	final String specialCharacter4 = "11";
	final String specialCharacter5 = "11";

	public String getRequestIdCommand() {
		StringBuilder builder = new StringBuilder();
		builder.append("01");
		builder.append("00000000000000000000");
		builder.append("00");
		builder.append("04");
		return builder.toString().toUpperCase();
	}

	public String[] getDeviceIdFromResponse(String response)  throws Exception{
		if ((!isValid(response)))
			return null;
		response = removeAndTreatSpecialCaracters(response);

		if(response.length() < 15){
			return null;
		}

		LogUtil.i("Response parsed: " + response);
		String[] toReturn = new String[5];
		toReturn[0] = response.substring(4, 6);
		LogUtil.i("ID 0 : " + toReturn[0]);
		toReturn[1] = response.substring(6, 8);
		LogUtil.i("ID 1 : " + toReturn[1]);
		toReturn[2] = response.substring(8, 10);
		LogUtil.i("ID 2 : " + toReturn[2]);
		toReturn[3] = response.substring(10, 12);
		LogUtil.i("ID 3 : " + toReturn[3]);
		toReturn[4] = response.substring(12, 14);
		LogUtil.i("ID 4 : " + toReturn[4]);
		return toReturn;
	}

	public String removeAndTreatSpecialCaracters(String response) {
		response = response.substring(2, response.length());
		response = response.substring(0, response.length() - 2);
		response = invertTreatCommandSpecialCharacters(response.toUpperCase());
		return response;
	}

	public String getMessageCommand(String[] ids, String message) {
		String header = "0000005F" + ids[0] + ids[1] + ids[2] + ids[3] + ids[4];
		String messageBody = "040000" + asciiToHex(message);
		header = treatCommandSpecialCharacters(header);
		messageBody += getMessageByteWord(messageBody);
		messageBody = treatCommandSpecialCharacters(messageBody);
		messageBody += treatCommandSpecialCharacters(sumCheckSum(header
				+ messageBody));
		StringBuilder builder = new StringBuilder();
		builder.append(specialCharacter);
		builder.append(header);
		builder.append(messageBody);
		builder.append(specialCharacter2);
		return builder.toString().toUpperCase();
	}

	public String getMessageFromResponse(String response) {
		LogUtil.i("Message to be parsed: " + response);
		if (!isValid(response)){
			LogUtil.i("A mensagem n��o �� v��lida");
			return null;
		}

		if(response.length() < 36)
			return null;

		response = response.toUpperCase();

		// Message toReturn = new Message();
		response = removeAndTreatSpecialCaracters(response);
		LogUtil.i("Message inverted: " + response);
		// Data e hora
		/*
		 * builder.append(response.substring(2,4));//horas
		 * builder.append(response.substring(4,6));//minutos
		 * builder.append(response.substring(6,8));//segundos
		 * builder.append(response.substring(8,10));//dia
		 * builder.append(response.substring(10,12));//mes
		 * builder.append(response.substring(12,14));//ano
		 */
		// IDENTIFICA����O
		// builder.append(hexToASCII(response.substring(14,18)));

		LogUtil.d("resposta antes da delimitação para trocar par aascii"+response);
		String toReturn = hexToASCII(response.substring(36, response.length() - 2));
		

		return toReturn;
	}

	public String getOpenRouteCommand(String[] ids, Integer routeId) {
		StringBuilder builder = new StringBuilder();
		builder.append("5B");
		builder.append(ids[0]);
		builder.append(ids[1]);
		builder.append(ids[2]);
		builder.append(ids[3]);
		builder.append(ids[4]);
		builder.append(getIntegerByteWord(routeId));
		String messageBody = treatCommandSpecialCharacters(builder.toString());
		messageBody += treatCommandSpecialCharacters(sumCheckSum(messageBody));
		builder.setLength(0);
		builder.append("01");
		builder.append("000000");
		builder.append(messageBody);
		builder.append("04");
		return builder.toString().toUpperCase();
	}

	public String getCloseRouteCommand(String[] ids) {
		StringBuilder builder = new StringBuilder();
		builder.append("5B");
		builder.append(ids[0]);
		builder.append(ids[1]);
		builder.append(ids[2]);
		builder.append(ids[3]);
		builder.append(ids[4]);
		builder.append(getIntegerByteWord(0));
		String messageBody = treatCommandSpecialCharacters(builder.toString());
		messageBody += treatCommandSpecialCharacters(sumCheckSum(messageBody));
		builder.setLength(0);
		builder.append("01");
		builder.append("000000");
		builder.append(messageBody);
		builder.append("04");
		return builder.toString().toUpperCase();
	}

	private String treatCommandSpecialCharacters(String command) {
		if (command == null)
			return null;
		String toReturn = "";
		int i = 0;
		String parsedString;
		while (i < command.length()) {
			i += 2;
			parsedString = command.substring(i - 2, i);
			parsedString = treatSpecialCharacter(parsedString);
			toReturn += parsedString;
		}
		return toReturn;
	}

	private String treatSpecialCharacter(String hexaCode) {
		if (specialCharacter.equals(hexaCode)) {
			return "1021";
		} else if (specialCharacter2.equals(hexaCode)) {
			return "1024";
		} else if (specialCharacter3.equals(hexaCode)) {
			return "1030";
		} else if (specialCharacter4.equals(hexaCode)) {
			return "1031";
		} else if (specialCharacter5.equals(hexaCode)) {
			return "1033";
		}
		return hexaCode;
	}

	private String invertTreatCommandSpecialCharacters(String command) {
		if (command == null)
			return null;
		command = command.replaceAll("1021", specialCharacter);
		command = command.replaceAll("1024", specialCharacter2);
		command = command.replaceAll("1030", specialCharacter3);
		command = command.replaceAll("1031", specialCharacter4);
		command = command.replaceAll("1033", specialCharacter5);
		return command;
	}

	private String sumCheckSum(String command) {
		if (command.length() % 2 != 0)
			return null;
		String[] cArray = command.split("");
		int i = 1;
		Integer checkSum = 0;
		String hexChar;
		while (i < command.length()) {
			hexChar = "";
			hexChar = cArray[i] + cArray[i + 1];
			checkSum += Integer.parseInt(hexChar, 16);
			i += 2;
		}
		String checkSumString = Integer.toHexString(checkSum).toUpperCase();
		String toReturn = checkSumString.substring(checkSumString.length() - 2,
				checkSumString.length());
		return toReturn;
	}

	public byte[] hexStringToByteArray(String s) {
		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
		}
		return data;
	}

	public String getHexString(byte[] raw) throws UnsupportedEncodingException {
		byte[] HEX_CHAR_TABLE = { (byte) '0', (byte) '1', (byte) '2',
				(byte) '3', (byte) '4', (byte) '5', (byte) '6', (byte) '7',
				(byte) '8', (byte) '9', (byte) 'a', (byte) 'b', (byte) 'c',
				(byte) 'd', (byte) 'e', (byte) 'f' };
		byte[] hex = new byte[2 * raw.length];
		int index = 0;

		for (byte b : raw) {
			int v = b & 0xFF;
			hex[index++] = HEX_CHAR_TABLE[v >>> 4];
			hex[index++] = HEX_CHAR_TABLE[v & 0xF];
		}
		return new String(hex, "ASCII");
	}

	private static String asciiToHex(String ascii) {
		StringBuilder hex = new StringBuilder();

		for (int i = 0; i < ascii.length(); i++) {
			hex.append(Integer.toHexString(ascii.charAt(i)));
		}
		return hex.toString();
	}

	private static String hexToASCII(String hex) {
		if (hex.length() % 2 != 0) {
			return null;
		}
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < hex.length() - 1; i += 2) {
			String output = hex.substring(i, (i + 2));
			int decimal = Integer.parseInt(output, 16);
			sb.append((char) decimal);
		}
		return sb.toString();
	}

	private String getMessageByteWord(String command) {
		String toReturn = "";
		Integer bytes = command.length() / 2;
		String hex = Integer.toHexString(bytes);

		if (hex.length() == 1) {
			toReturn = "000" + hex;
		} else if (hex.length() == 2) {
			toReturn = "00" + hex;
		} else if (hex.length() == 3) {
			toReturn = "0" + hex;
		} else if (hex.length() >= 4) {
			toReturn = hex.substring(0, 4);
		}

		return toReturn.toUpperCase();
	}

	private String getIntegerByteWord(Integer value) {
		String toReturn = "";
		String hex = Integer.toHexString(value);
		if (hex.length() == 1) {
			toReturn = "000" + hex;
		} else if (hex.length() == 2) {
			toReturn = "00" + hex;
		} else if (hex.length() == 3) {
			toReturn = "0" + hex;
		} else if (hex.length() >= 4) {
			toReturn = hex.substring(0, 4);
		}
		return toReturn.toUpperCase();
	}

	public boolean isValid(String response) {
		if (response == null || response.length() < 4)
			return false;
		if (!response.subSequence(0, 2).equals("01"))
			return false;
		if (response.lastIndexOf("04") == -1)
			return false;

		return true;
	}

}
