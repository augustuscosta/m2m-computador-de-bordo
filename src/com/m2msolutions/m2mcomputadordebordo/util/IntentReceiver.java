package com.m2msolutions.m2mcomputadordebordo.util;

import android.app.Activity;
import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class IntentReceiver extends BroadcastReceiver {
	
	public void onReceive(Context context, Intent intent) {		
		KeyguardManager keyguardManager = ((KeyguardManager)context.getSystemService(Activity.KEYGUARD_SERVICE));
        KeyguardLock lock = keyguardManager.newKeyguardLock(Context.KEYGUARD_SERVICE);
        lock.disableKeyguard();
	}
	
}