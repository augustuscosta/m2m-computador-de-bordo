package com.m2msolutions.m2mcomputadordebordo.util;

public enum BroadcastDictionary {
	
	Zero("01");
	
	private String value;
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	private BroadcastDictionary(String value){
		this.value = value;
	}

}
