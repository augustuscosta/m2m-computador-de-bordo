package com.m2msolutions.m2mcomputadordebordo.util;

import com.m2msolutions.m2mcomputadordebordo.database.ConfiguracaoDAO;
import com.m2msolutions.m2mcomputadordebordo.database.MessageDAO;
import com.m2msolutions.m2mcomputadordebordo.database.TrigeredEventDao;
import com.m2msolutions.m2mcomputadordebordo.model.Configuracao;

import android.content.Context;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;

public class Session {

	
	public static final String PREFS 						 = "SESSION_PREFS";
	public static final String IDKEY 						 = "ID_KEY";
	public static final String IDROUTE 						 = "IDROUTE";
	public static final String NAMEROUTE 					 = "NAMEROUTE";
	public static final	String IDDRIVER						 ="DRIVER_ID";
	public static final	String CONFIG_PASSWORD				 ="CONFIG_PASSWORD";
	public static final	String CONFIG_TIME					 ="CONFIG_TIME";
	public static final	String IN_SESSION					 ="IN_SESSION";

	private static SharedPreferences settings;
	
	
	private static SharedPreferences getSharedPreferencesInstance(Context context){
		if(settings == null){
			settings = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
		}
		return settings;
	}
	
	public static String getDeviceImei(Context context){
		TelephonyManager TelephonyMgr = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
		String szImei = TelephonyMgr.getDeviceId();
		return szImei;
	}
	
	
	public static void setDeviceId(String[] ids,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		if(ids == null){
			clearIds();
			return;
		}
		editor.putString(IDKEY, ids[0]);
		editor.putString(IDKEY+"1", ids[1]);
		editor.putString(IDKEY+"2", ids[2]);
		editor.putString(IDKEY+"3", ids[3]);
		editor.putString(IDKEY+"4", ids[4]);
		editor.commit();
	}

	public static void clearIds() {
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(IDKEY, null);
		editor.putString(IDKEY+"1", null);
		editor.putString(IDKEY+"2", null);
		editor.putString(IDKEY+"3", null);
		editor.putString(IDKEY+"4", null);
		editor.commit();
	}
	
 	public static String[] getDeviceId(Context context){
 		String[] toReturn = new String[5];
 		toReturn[0] = getSharedPreferencesInstance(context).getString(IDKEY, null);
 		toReturn[1] = getSharedPreferencesInstance(context).getString(IDKEY+"1", null);
 		toReturn[2] = getSharedPreferencesInstance(context).getString(IDKEY+"2", null);
 		toReturn[3] = getSharedPreferencesInstance(context).getString(IDKEY+"3", null);
 		toReturn[4] = getSharedPreferencesInstance(context).getString(IDKEY+"4", null);
		return checkIds(toReturn);
	}

	private static String[] checkIds(String[] toReturn) {
		if(toReturn[0] == null || toReturn[1] == null || toReturn[2] == null || toReturn[3] == null || toReturn[4] == null)
			return null;
		return toReturn;
	}
	
	public static void setRouteId(Integer routeId,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		if(routeId == null) routeId=-1;
		editor.putInt(IDROUTE, routeId);
		editor.commit();
	}
	
	public static Integer getRouteId(Context context){
		Integer toReturn = getSharedPreferencesInstance(context).getInt(IDROUTE, -1);
		if(toReturn == -1)
			return null;
		return toReturn;
	}
	
	public static String getRouteName(Context context){
		return getSharedPreferencesInstance(context).getString(NAMEROUTE, null);
	}

	public static void setRouteName(String name,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(NAMEROUTE, name);
		editor.commit();
	}
	
	public static void setDriverId(Long driverId,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putLong(IDDRIVER, driverId);
		editor.commit();
	}
	
	public static Long getDriverId(Context context){
		Long toReturn = getSharedPreferencesInstance(context).getLong(IDDRIVER, -1);
		if(toReturn == -1)
			return null;
		return toReturn;
	}
	
	public static void setConfigTime(Long configTime,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putLong(CONFIG_TIME, configTime);
		editor.commit();
	}
	
	public static Long getConfigTime(Context context){
		Long toReturn = getSharedPreferencesInstance(context).getLong(CONFIG_TIME,-1);
		if(toReturn == -1)
			return null;
		return toReturn;
	}
	
	public static void logOff(Context context){
		SharedPreferences.Editor editor = settings.edit();
		new MessageDAO(context).deleteAll();
		new TrigeredEventDao(context).deleteAll();
		editor.putBoolean(IN_SESSION, false);
		editor.putLong(IDDRIVER, -1);
		editor.putString(NAMEROUTE, "");
		editor.putInt(IDROUTE, -1);
		editor.commit();
	}
	
	public static void switchDriver(Context context){
		SharedPreferences.Editor editor = settings.edit();
		editor.putLong(IDDRIVER, -1);
		editor.commit();
	}
	
	public static void setConifgFromDb(Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		ConfiguracaoDAO dao = new ConfiguracaoDAO(context);
		Configuracao config = dao.get();
		if(config!= null && config.getMasterPassword()!= null){
			editor.putString(CONFIG_PASSWORD, config.getMasterPassword());
		}else{
			editor.putString(CONFIG_PASSWORD, "0000");			
		}
		editor.commit();
	}
	
	public static String getMasterPassword(Context context){
		return getSharedPreferencesInstance(context).getString(CONFIG_PASSWORD, "0000");
	}
	
	public static void setInSession(Context context, boolean inSession){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(IN_SESSION, inSession);
		editor.commit();
	}
	
	public static boolean getInSession(Context context){
		return getSharedPreferencesInstance(context).getBoolean(IN_SESSION, false);
	}
}
