package com.m2msolutions.m2mcomputadordebordo.util;

public class StringUtil {

	/**
	 * @param value
	 * @return True if value is not null and trim().length() > 0, otherwise false.
	 */
	public static boolean isValid(final String value) {
		return value != null && value.trim().length() > 0;
	}
	
	public static int indexOfAnyBut(String str, String searchChars) {

	      for (int i = 0; i < str.length(); i++) {
	          if (searchChars.indexOf(str.charAt(i)) < 0) {
	              return i;
	          }
	      }
	      return -1;
	  }
	
}
