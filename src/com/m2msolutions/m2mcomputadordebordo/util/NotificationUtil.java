package com.m2msolutions.m2mcomputadordebordo.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.m2msolutions.m2mcomputadordebordo.R;
import com.m2msolutions.m2mcomputadordebordo.model.TrigeredEvent;

public class NotificationUtil {

	public static final int NOTIFICATION_ID = 1;
	
	public static void notifyEvento(Context context, TrigeredEvent evento){
		NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		CharSequence contentTitle = context.getString(R.string.app_name);
		CharSequence contentText = evento.getEvento().getTexto();
		Notification notification = new Notification(R.drawable.logo_m2m_01, "Evento", System.currentTimeMillis());
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0, new Intent(), 0);
		notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);
		notification.sound = Uri.parse(context.getFileStreamPath(evento.getEvento().getId()+".mp3").getPath());
		notification.defaults = Notification.DEFAULT_SOUND;
		mNotificationManager.notify(NOTIFICATION_ID, notification);
		mNotificationManager.cancel(NOTIFICATION_ID);
	}

}
