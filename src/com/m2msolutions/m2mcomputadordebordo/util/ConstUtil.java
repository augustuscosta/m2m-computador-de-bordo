package com.m2msolutions.m2mcomputadordebordo.util;


public class ConstUtil {

	public static final int SECURITY_REQUEST = 2243;
	public static final int LOG_OFF_REQUEST = 2244;
	public static final int MENU_REQUEST = 2245;
	public static final int SPLASHSCREEN = 2246;
	public static final int BUS_SERVICE = 2247;
	public static final int EVENT_REQUEST = 2448;
	public static final int BUS_CHAT= 2249;
	public static final String PNG = ".png";
	public static final String ANSWER = "answer";
	public static final int MESSAGE_NOTIFICATION_REQUEST = 2250;
	public static final int HELP_REQUEST = 2251;
	public static final int EVENTO_NOTIFICATION_REQUEST = 2251;
	public static final int SINOTICO_REQUEST = 2252;
	public static final String SERIALIZABLE_KEY = "SERIALIZABLE_KEY";
	
}
