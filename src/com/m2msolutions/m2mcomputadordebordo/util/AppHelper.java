package com.m2msolutions.m2mcomputadordebordo.util;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Address;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.Settings.Secure;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;

import com.m2msolutions.m2mcomputadordebordo.R;
import com.m2msolutions.m2mcomputadordebordo.database.DaoProvider;
import com.m2msolutions.m2mcomputadordebordo.model.Message;
import com.m2msolutions.m2mcomputadordebordo.model.MessageOwner;
import com.m2msolutions.m2mcomputadordebordo.model.TrigeredEvent;
import com.m2msolutions.m2mcomputadordebordo.sdcardfiles.soundsandpics.SoundAndPicturesTransfer;
import com.m2msolutions.m2mcomputadordebordo.sdcardfiles.xml.XMLFileReader;
import com.m2msolutions.m2mcomputadordebordo.service.SerialPortService.ISerialPortService;

public class AppHelper extends Application {

	private static AppHelper instance = null;
	private int index = 0;

	public static AppHelper getInstance() {
		if (instance != null) {
			return instance;
		} else {
			instance = new AppHelper();
			return instance;
		}
	}

	public static Point getDisplaySize(Context context){
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		Point size = new Point();
		size.set(display.getWidth(), display.getHeight());
		return size;
	}

	public String formateDatewithTime(Activity context, Long date){
		String formated ="";
		DateFormat dateFormat = android.text.format.DateFormat
				.getDateFormat(context);
		DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		formated =dateFormat.format(new Date(date));
		formated += " ";
		formated += timeFormat.format(new Date(date));
		return formated;
	}

	public String getFormatedTime(){
		String formated ="";
		DateFormat timeFormat = new SimpleDateFormat("HH:mm");
		formated += timeFormat.format(new Date());
		return formated;
	}

	public String formatTime(Date date){
		String formated ="";
		DateFormat timeFormat = new SimpleDateFormat("HH:mm");
		formated += timeFormat.format(date);
		return formated;
	}
	public String formatTime(Long number){
		Date date = new Date(number);
		String formated ="";
		DateFormat timeFormat = new SimpleDateFormat("HH:mm");
		formated += timeFormat.format(date);
		return formated;
	}

	public void presentError(Context ctx, String title, String description) {
		AlertDialog.Builder d = new AlertDialog.Builder(ctx);
		d.setTitle(title);
		d.setMessage(description);
		d.setIcon(android.R.drawable.ic_dialog_alert);
		d.setNeutralButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
			}
		});
		d.show();
	}


	public String getRealPathFromURI(Uri contentUri, Activity context) {
		String[] proj = { MediaStore.Images.Media.DATA };
		Cursor cursor = context.managedQuery(contentUri, proj, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	public String decodeUri(String myUri, Activity context){
		Uri uri =  Uri.parse(myUri);
		return getRealPathFromURI(uri, context);
	}



	public String getUniqueDeviceID(Context context) {
		return Secure
				.getString(context.getContentResolver(), Secure.ANDROID_ID);
	}

	public byte[] parseBitmapToByteArray(Bitmap bitmap) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		if (bitmap.compress(CompressFormat.JPEG, 76, bos)) {
			return bos.toByteArray();
		}

		return null;
	}

	@Override
	public void onTerminate() {
		instance = null;
		super.onTerminate();
	}

	public static Long getCurrentTime(){
		return new Date().getTime();
	}

	public static Date getCurrentDate(){
		return new Date();
	}

	public String getAddressLineFromGoogleAddress(Address address) {
		String toReturn = "";
		if (address == null) {
			return toReturn;
		}
		for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
			toReturn += address.getAddressLine(i) + " ";
		}
		return toReturn;
	}

	public boolean checkConnection(Context context) {
		ConnectivityManager conMgr = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo i = conMgr.getActiveNetworkInfo();
		if (i == null)
			return false;
		if (!i.isConnected())
			return false;
		if (!i.isAvailable())
			return false;
		return true;
	}


	public boolean isOnWifi(Context context) {
		ConnectivityManager connManager = (ConnectivityManager) context
				.getSystemService(CONNECTIVITY_SERVICE);
		NetworkInfo mWifi = connManager
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

		if (mWifi.isConnected()) {
			return true;
		}

		return false;
	}

	public void paintTextRed(TextView textView) {
		textView.setTextColor(Color.RED);

	}

	public void paintTextRegular(TextView textView){
		textView.setTextColor(Color.BLACK);
		textView.invalidate();
	}

	public String getAppVersion(Context context){
		String version = "";
		try {
			version = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			LogUtil.e("Erro ao pegar a versao", e);
		}
		return version;
	}

	public static void unbindDrawables(View view) {
		if (view.getBackground() != null) {
			view.getBackground().setCallback(null);
		}
		if (view instanceof ViewGroup && !(view instanceof AdapterView)) {
			for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
				unbindDrawables(((ViewGroup) view).getChildAt(i));
			}
			((ViewGroup) view).removeAllViews();
		}
	}

	public static void importAllDataFromSdCard(Context context) throws Exception{
		DaoProvider.cleanData(context);
		XMLFileReader.readAndSaveAll(context);
		SoundAndPicturesTransfer.copyAll(context);
	}

	public static void showDeviceIdError(Context context) {
		new AlertDialog.Builder(context)
		.setIcon(android.R.drawable.ic_dialog_info)
		.setTitle(R.string.erro)
		.setMessage(R.string.device_id_not_found)
		.setCancelable(false)
		.setNegativeButton(R.string.OK,
				new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog,
					int which) {
				dialog.cancel();
			}
		}).show();
	}



	public  void presentNewMessage(final Context context,final  ISerialPortService service, final List<Message> messages){
		if(messages == null || messages.isEmpty()) return;
		
		index = messages.size()-1;
		final Dialog messageDialog = new Dialog(context);
		messageDialog.setContentView(R.layout.message_dialog);
		messageDialog.setTitle(R.string.mensagem);
		messageDialog.setCancelable(false);
		final TextView guiaNumber = (TextView) messageDialog.findViewById(R.id.message_body);
		guiaNumber.setText(messages.get(index).getMessage());
		final Button responseButton  = (Button) messageDialog.findViewById(R.id.quick_response);
		final Button previousButton  = (Button) messageDialog.findViewById(R.id.previous);
		final Button nextButton  = (Button) messageDialog.findViewById(R.id.next);
		responseButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if(Session.getDeviceId(context) == null){
					showDeviceIdError(context);
					messageDialog.cancel();
				}

				if(service != null){
					Message message2 = new Message();
					message2.setDate(new Date(AppHelper.getCurrentTime()));
					message2.setMessage("Ok");
					message2.setMessageOwner(MessageOwner.MOTORISTA);
					service.sendMessage(message2);					
				}

				messageDialog.cancel();
			}
		});	
		previousButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if(index != 0){
					guiaNumber.setText(messages.get(index-1).getMessage());
					index = index-1;
				}
			}
		});	

		nextButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if(index < messages.size()-1){
					guiaNumber.setText(messages.get(index+1).getMessage());
					index = index +1;
				}
			}

	});	
		messageDialog.show();

}
	public  static void presentNewEvento(final Context context, final TrigeredEvent evento){
		
		final Dialog eventDialog = new Dialog(context);
		eventDialog.setContentView(R.layout.evento_dialog);
		eventDialog.setTitle(R.string.event);
		eventDialog.setCancelable(false);
		final TextView eventoText = (TextView) eventDialog.findViewById(R.id.message_body);
		if(evento.getEvento() != null){
			eventoText.setText(evento.getEvento().getTexto());			
		}
		final Button responseButton  = (Button) eventDialog.findViewById(R.id.quick_response);
		responseButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				eventDialog.cancel();
			}
		});	
		eventDialog.show();
		
	}
	
	public static boolean acceptKey(final int keyCode) {
		if ((KeyEvent.KEYCODE_SEARCH == keyCode)) {

			LogUtil.d("Kiosk mode is enabled, block keyPress");
			return true;
		}

		return false;
	}

	public static boolean acceptKeyKioskMode(final int keyCode) {
		if ( KeyEvent.KEYCODE_HOME == keyCode ) {
			
			LogUtil.d("Kiosk mode is enabled, block KeyHome...");
			return true;
		}
		
		return false;
	}
	
	public static boolean acceptKeyKioskModePower(final int keyCode) {
		if ( KeyEvent.KEYCODE_POWER == keyCode ) {
			
			LogUtil.d("Kiosk mode is enabled, block KeyPower...");
			return true;
		}
		
		return false;
	}

}
