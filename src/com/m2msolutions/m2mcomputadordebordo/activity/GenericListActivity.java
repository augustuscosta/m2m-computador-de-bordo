package com.m2msolutions.m2mcomputadordebordo.activity;

import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.BitmapDrawable;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.m2msolutions.m2mcomputadordebordo.sdcardfiles.soundsandpics.SoundAndPicturesTransfer;
import com.m2msolutions.m2mcomputadordebordo.util.AppHelper;
import com.m2msolutions.m2mcomputadordebordo.util.ConstUtil;
import com.m2msolutions.m2mcomputadordebordo.util.Session;

public class GenericListActivity extends ListActivity {

	protected ImageView fotoMotorista;
	protected TextView relogio;
	protected TextView linhaTextView;


	@Override
	protected void onResume() {
		super.onResume();
		registerReceiver(timeBroadCastReceiver, new IntentFilter(
				Intent.ACTION_TIME_TICK));
		updateTime();
		loadMotoristaPic();
		setRouteTittle();
	}

	@Override
	protected void onPause() {
		unregisterReceiver(timeBroadCastReceiver);
		super.onPause();
	}

	//	KIOSKE MODE THINGS

	@Override
	public boolean onSearchRequested() {
		return false;
	}

	@Override
	public boolean onKeyLongPress(int keyCode, KeyEvent event) {
		if ( AppHelper.acceptKey(keyCode) && AppHelper.acceptKeyKioskMode(keyCode)&& AppHelper.acceptKeyKioskModePower(keyCode) ) {
			return true;
		}

		return super.onKeyLongPress(keyCode, event);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ( AppHelper.acceptKey(keyCode) && AppHelper.acceptKeyKioskMode(keyCode)&& AppHelper.acceptKeyKioskModePower(keyCode) ) {
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if ( AppHelper.acceptKey(keyCode) && AppHelper.acceptKeyKioskMode(keyCode)&& AppHelper.acceptKeyKioskModePower(keyCode) ) {
			return true;
		}
		
		return super.onKeyUp(keyCode, event);
	}


	@Override
	public void onBackPressed() {
		// DO Noting
	}

	// MENU SECURITY

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		
		return true;
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		if (requestCode == ConstUtil.SECURITY_REQUEST && resultCode == RESULT_OK) {
			startActivity(new Intent(GenericListActivity.this,
					SettingsActivity.class));
		}

		if (resultCode == ConstUtil.LOG_OFF_REQUEST) {
			setResult(ConstUtil.LOG_OFF_REQUEST);
			finish();
		}


	}

	//Time update sequence

	final private BroadcastReceiver timeBroadCastReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			updateTime();
		}

	};

	protected void updateTime() {
		if(relogio != null) relogio.setText(AppHelper.getInstance().getFormatedTime());
	}

	//Header settings
	protected void loadMotoristaPic() {
		BitmapDrawable bitmapDrawable =SoundAndPicturesTransfer.loadMotoristaPic(this);
		if(bitmapDrawable != null && fotoMotorista != null)	fotoMotorista.setBackgroundDrawable(bitmapDrawable);
	}

	protected void setRouteTittle(){
		if(Session.getRouteId(this) != null  && linhaTextView != null){
			linhaTextView.setText(Session.getRouteName(this));
		}else{
			if(linhaTextView != null)	linhaTextView.setText("");				
		}
	}

	public void onClickMenu(View view){
		startActivityForResult(new Intent(this, MenuActivity.class ),ConstUtil.MENU_REQUEST);
	}

	public void onClickLogOff(View view){
		startActivityForResult(new Intent(this,LogoOffMenu.class), ConstUtil.LOG_OFF_REQUEST);
	}

	public void onClickLine(View view){
		startActivityForResult(new Intent(this,BusServiceListActivity.class), ConstUtil.BUS_SERVICE);				
	}


}
