package com.m2msolutions.m2mcomputadordebordo.activity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.m2msolutions.m2mcomputadordebordo.R;
import com.m2msolutions.m2mcomputadordebordo.service.SerialPortService;
import com.m2msolutions.m2mcomputadordebordo.util.Session;

public class MainActivity extends GenericActivity {
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_new);
        configureActivity();
        SerialPortService.startService(this);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if(Session.getDriverId(this) == null) finish();
	}
	
	private void configureActivity(){
		fotoMotorista = (ImageView) findViewById(R.id.fotoMotorissta);
		relogio = (TextView) findViewById(R.id.relogio);
		linhaTextView = (TextView) findViewById(R.id.linha);
	}
	
	@Override
	protected void onDestroy() {
		SerialPortService.stopService(this);
		super.onDestroy();
	}
	
	
	@Override
	public void onBackPressed() {
		return;
	}
}
