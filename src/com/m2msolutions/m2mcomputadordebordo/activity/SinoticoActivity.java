package com.m2msolutions.m2mcomputadordebordo.activity;

import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.m2msolutions.m2mcomputadordebordo.R;
import com.m2msolutions.m2mcomputadordebordo.database.RotaDAO;
import com.m2msolutions.m2mcomputadordebordo.model.Ponto;
import com.m2msolutions.m2mcomputadordebordo.model.Rota;
import com.m2msolutions.m2mcomputadordebordo.model.VehicleStatus;
import com.m2msolutions.m2mcomputadordebordo.util.AppHelper;
import com.m2msolutions.m2mcomputadordebordo.util.Session;

public class SinoticoActivity extends GenericSerialPortActivity {
	
	private static final String PERCENTUAL = " %";
	private static final String KM = " km";
	private TextView partidaBox;
	private TextView tempoSaidaBox;
	private TextView tempoProximaParada;
	private TextView paradaAtualBox;
	private TextView tempoPercorridoBox;
	private TextView tempoChegadaBox;
	private TextView distanciaPercorridaBox;
	private TextView percentualBox;
	private TextView chegadaBox;
	private TextView previsaoChegadaBox;
	private TextView diatanciaFinalBox;
	private Rota rota;
	private RotaDAO rotaDAO;

	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sinotico_development);
        configureActivity();
	}
	
	@Override
	protected void onResume() {
//		checkCurrentRoute();
		super.onResume();
	}
	
	private void configureActivity(){
		fotoMotorista 			= (ImageView) findViewById(R.id.fotoMotorissta);
		relogio				    = (TextView) findViewById(R.id.relogio);
		linhaTextView 			= (TextView) findViewById(R.id.linha);
//		partidaBox 				= (TextView) findViewById(R.id.partidaBox);
//		tempoSaidaBox 			= (TextView) findViewById(R.id.tempoSaidaBox);
//		tempoProximaParada  	= (TextView) findViewById(R.id.tempoProximaParada);
//		paradaAtualBox  		= (TextView) findViewById(R.id.paradaAtualBox);
//		tempoPercorridoBox 		= (TextView) findViewById(R.id.tempoPercorridoBox);
//		tempoChegadaBox 		= (TextView) findViewById(R.id.tempoChegadaBox);
//		distanciaPercorridaBox  = (TextView) findViewById(R.id.distanciaPercorridaBox);
//		percentualBox		    = (TextView) findViewById(R.id.percentualBox);
//		chegadaBox  			= (TextView) findViewById(R.id.chegadaBox);
//		previsaoChegadaBox  	= (TextView) findViewById(R.id.previsaoChegadaBox);
//		diatanciaFinalBox 		= (TextView) findViewById(R.id.diatanciaFinalBox);
	}
	
	private void checkCurrentRoute() {
		if(Session.getRouteId(this) == null){
			showNoRouteSelectedError();
			return;
		}
		getRouteAndShowOnView((Session.getRouteId(this)));
	}

	private void showNoRouteSelectedError() {
			new AlertDialog.Builder(this)
			.setIcon(android.R.drawable.ic_dialog_info)
			.setTitle(R.string.erro)
			.setMessage(R.string.no_route)
			.setCancelable(false)
			.setNegativeButton(R.string.OK,
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					finish();
				}
			}).show();
	}
	
	private void getRouteAndShowOnView(Integer id) {
		rota = rotaDAO().findRota(id);
		populateFields();
	}

	private void populateFields() {
		List<Ponto> pontos =  rota.getPontos();
		if(pontos == null || pontos.isEmpty()){
			return;
		}
		
//		setDataOnFields(pontos);
	}

	private void setDataOnFields(List<Ponto> pontos) {
		partidaBox.setText(pontos.get(0).getNome());
		chegadaBox.setText(pontos.get(rota.getPontos().size()-1).getNome());
		setLastPonto();
		setStartedTime();
//		tempoProximaParada.setText(getNextStopTime(pontos));
//		tempoPercorridoBox.setText(getElapsedTime());
//		tempoChegadaBox.setText(getPreviewTime());
//		distanciaPercorridaBox.setText(getDistanciaPercorrida(pontos));
//		percentualBox.setText(getPercentualPercorrido(pontos));
//		previsaoChegadaBox.setText(getPrevisaoChegadaTime());
//		diatanciaFinalBox.setText(getDistanciaFinal(pontos));
	}

	private void setStartedTime() {
		if(rota.getStartedAt() != null)
		tempoSaidaBox.setText(AppHelper.getInstance().formatTime(rota.getStartedAt()));
	}

	private void setLastPonto() {
		if(rota.getLastPonto()!= null)
		paradaAtualBox.setText(rota.getLastPonto().getNome());
	}

	private CharSequence getDistanciaFinal(List<Ponto> pontos) {
		double distance = rota.getTotalKm();
		distance -= getElapsedDistance(pontos);
		return Double.toString(distance);
	}

	private CharSequence getPercentualPercorrido(List<Ponto> pontos) {
		double percentual = 0;
		if(rota.getLastPonto() == null) return Double.toString(percentual) + PERCENTUAL;
		
		for(Ponto ponto : pontos){
			if(!ponto.equals(rota.getLastPonto())){
				percentual += ponto.getFraction();
			}else{
				percentual += ponto.getFraction();
				break;
			}
		}
		
		percentual = percentual * 100;
		return Double.toString(percentual) +PERCENTUAL;
	}

	private CharSequence getPrevisaoChegadaTime() {
		long previewTime = rota.getStartedAt().getTime()+rota.getTotalTempo() - AppHelper.getCurrentTime();
		return AppHelper.getInstance().formatTime(previewTime);
	}

	private CharSequence getDistanciaPercorrida(List<Ponto> pontos) {
		double distance = 0;
		if(rota.getLastPonto() == null) return Double.toString(distance) +KM;
		
		distance = getElapsedDistance(pontos);
		return Double.toString(distance) +KM;
	}

	private double getElapsedDistance(List<Ponto> pontos) {
		double distance = 0;
		for(Ponto ponto : pontos){
			if(!ponto.equals(rota.getLastPonto())){
				distance += rota.getTotalKm()*ponto.getFraction();
			}else{
				distance += rota.getTotalKm()*ponto.getFraction();
				break;
			}
		}
		return distance;
	}

	private CharSequence getPreviewTime() {
		long previewdTime = rota.getStartedAt().getTime()+rota.getTotalTempo();		
		return AppHelper.getInstance().formatTime(previewdTime);
	}


	private CharSequence getElapsedTime() {
		long elapsedTime = AppHelper.getCurrentTime() - rota.getStartedAt().getTime();
		return AppHelper.getInstance().formatTime(elapsedTime);
	}


	private String getNextStopTime(List<Ponto> pontos) {
		Ponto ponto= pontos.get(pontos.indexOf(rota.getLastPonto()));
		Long tempopPraPonto  = (long) (rota.getStartedAt().getTime()+(rota.getTotalTempo()*ponto.getFraction()));
		String toReturn = AppHelper.getInstance().formatTime(tempopPraPonto);
		return toReturn;
	}

	private RotaDAO rotaDAO() {
		if(rotaDAO == null){
			rotaDAO = new RotaDAO(this);
		}
		
		return rotaDAO;
	}
	
	@Override
	protected void processVehicleStatus(VehicleStatus vehicleStatus) {
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				
			}
		});
	}
	
	@Override
	public void onClickMenu(View view) {
		finish();
	}
}
