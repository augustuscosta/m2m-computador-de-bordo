package com.m2msolutions.m2mcomputadordebordo.activity;

import java.util.Date;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.m2msolutions.m2mcomputadordebordo.R;
import com.m2msolutions.m2mcomputadordebordo.adapter.MessageAdapter;
import com.m2msolutions.m2mcomputadordebordo.adapter.PreMadeMessageAdapter;
import com.m2msolutions.m2mcomputadordebordo.database.MensagemPreDao;
import com.m2msolutions.m2mcomputadordebordo.database.MessageDAO;
import com.m2msolutions.m2mcomputadordebordo.model.MensagemPre;
import com.m2msolutions.m2mcomputadordebordo.model.Message;
import com.m2msolutions.m2mcomputadordebordo.model.MessageOwner;
import com.m2msolutions.m2mcomputadordebordo.model.Rota;
import com.m2msolutions.m2mcomputadordebordo.model.TrigeredEvent;
import com.m2msolutions.m2mcomputadordebordo.model.VehicleStatus;
import com.m2msolutions.m2mcomputadordebordo.protocol.MaxtrackProtocol;
import com.m2msolutions.m2mcomputadordebordo.service.SerialPortService;
import com.m2msolutions.m2mcomputadordebordo.service.SerialPortService.ISerialPortListener;
import com.m2msolutions.m2mcomputadordebordo.service.SerialPortService.ISerialPortService;
import com.m2msolutions.m2mcomputadordebordo.service.SerialPortService.SerialPortBinder;
import com.m2msolutions.m2mcomputadordebordo.util.AppHelper;
import com.m2msolutions.m2mcomputadordebordo.util.ConstUtil;
import com.m2msolutions.m2mcomputadordebordo.util.LogUtil;
import com.m2msolutions.m2mcomputadordebordo.util.NotificationUtil;
import com.m2msolutions.m2mcomputadordebordo.util.Session;

public class BusChatActivity extends GenericListActivity{

	private EditText inputTextView;
	private ISerialPortService serialPortService;
	private Button chatButton;
	private Button cancelButton;
	Handler mHandler = new Handler();
	MaxtrackProtocol protocol = new MaxtrackProtocol();
	private List<Message> mensagens;
	private MessageAdapter adapter;
	private MessageDAO dao;
	InputMethodManager imm;
	private LinearLayout chatContainer;
	
	String message = "";
	private LinearLayout fixedHeader;
	private Button msgPreButton;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bus_chat);
		message = "";
		configureActivity();
		dao = new MessageDAO(this);
		inputTextView.setOnEditorActionListener(new OnEditorActionListener() {
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE) {
					if(Session.getDeviceId(BusChatActivity.this) == null){
						showDeviceIdError();			
					}else{
						validateAndSend();						
					}
					return true;
				}
				
				return true;
			}
		});
		handleIntent();
		mHandler = new Handler();
	}

	private void handleIntent() {
		boolean openKeyBoard = getIntent().getBooleanExtra(ConstUtil.ANSWER, false);
		if(openKeyBoard == true){
			openKeyboard();
		}
		
	}

	private void configureActivity() {
		imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		chatButton = (Button) findViewById(R.id.chatButton);
		inputTextView = (EditText) findViewById(R.id.inputTextView);
		fotoMotorista = (ImageView) findViewById(R.id.fotoMotorissta);
		relogio = (TextView) findViewById(R.id.relogio);
		linhaTextView = (TextView) findViewById(R.id.linha);
		cancelButton = (Button) findViewById(R.id.cancelButton);
		fixedHeader = (LinearLayout) findViewById(R.id.fixedHeader);
		chatContainer = (LinearLayout) findViewById(R.id.chatContainer);
		msgPreButton = (Button) findViewById(R.id.msPreButton);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		bindService(new Intent(SerialPortService.SERIAL_PORT_SERVICE_INTENT), serviceConnection, BIND_AUTO_CREATE);	
		setRouteTittle();
		refresh();
	}

	@Override
	protected void onPause() {
		unbindSerialPortService();
		super.onPause();
	}
	
	private void unbindSerialPortService() {
		if(serialPortService != null) serialPortService.removeListener(listener);
		unbindService(serviceConnection);
		serialPortService = null;
	}


	private void refresh() {
		mensagens = dao.getAll();
		adapter = new MessageAdapter(mensagens, this);
		setListAdapter(adapter);
		if(mensagens != null && !mensagens.isEmpty()) setSelection(mensagens.size()-1);
	}

	private void showDeviceIdError() {
		new AlertDialog.Builder(this)
		.setIcon(android.R.drawable.ic_dialog_info)
		.setTitle(R.string.erro)
		.setMessage(R.string.device_id_not_found)
		.setCancelable(false)
		.setNegativeButton(R.string.OK,
				new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog,
					int which) {
				finish();
			}
		}).show();
	}

	private void updateChat(Message message) {
		dao.persist(message);
		refresh();
	}

	private void sendMsg(String msg) {
		String command = protocol.getMessageCommand(Session.getDeviceId(this),msg);
		LogUtil.i("HEXA DO COMANDO ENVIADO: " + command);
		final Message message = new Message();
		message.setDate(new Date(AppHelper.getCurrentTime()));
		message.setMessage(msg);
		message.setMessageOwner(MessageOwner.MOTORISTA);
		sendMessage(message);
		updateChat(message);
	}

	public void sendButonClick(View v) {
		if(Session.getDeviceId(this) == null){
			showDeviceIdError();			
		}
			validateAndSend();			
	}

	private void openKeyboard() {
		inputTextView.setVisibility(View.VISIBLE);
		cancelButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.teclado_up));
		fixedHeader.setVisibility(View.GONE);
		msgPreButton.setVisibility(View.GONE);
		chatButton.setVisibility(View.VISIBLE);
		chatContainer.setVisibility(View.VISIBLE);
		inputTextView.setSelected(true);
		inputTextView.requestFocus();
		imm.showSoftInput(inputTextView, InputMethodManager.SHOW_FORCED);
		if(mensagens != null && !mensagens.isEmpty()) setSelection(mensagens.size()-1);
				
	}

	private void validateAndSend() {
		String msg = inputTextView.getText().toString();
		if (msg != null && msg.length() > 0) {
			sendMsg(msg);
			inputTextView.setText("");
		}
	}

	public void onClickGetMsgPre(View view){
		if(fixedHeader.getVisibility() == View.GONE){
			closeKeyBoard();
		}else{
			openKeyboard();
		}
	}

	private void closeKeyBoard() {
		fixedHeader.setVisibility(View.VISIBLE);
		chatButton.setVisibility(View.INVISIBLE);
		inputTextView.setVisibility(View.INVISIBLE);
		chatContainer.setVisibility(View.GONE);
		msgPreButton.setVisibility(View.VISIBLE);
		cancelButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.teclado_down));
		imm.hideSoftInputFromWindow(inputTextView.getWindowToken(), 0);
	}


	public void onClickShowPreDialog(View view){
		showMessagePreDialog();
	}
	
	private void showMessagePreDialog(){
		closeKeyBoard();
	   	final Dialog msgPreDialog = new Dialog(this);
		final MensagemPreDao dao = new MensagemPreDao(this);
		msgPreDialog.setContentView(R.layout.msg_pre_dialog);
		msgPreDialog.setTitle(getString(R.string.pre_message));
		ListView listView = (ListView) msgPreDialog.findViewById(R.id.mensagemPreList);
		final Button button = (Button) msgPreDialog.findViewById(R.id.cancelDialog);
		final List<MensagemPre> msgs = dao.getAllFromDataBase();
		PreMadeMessageAdapter adapter = new PreMadeMessageAdapter(this, msgs);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				if(Session.getDeviceId(BusChatActivity.this) == null){
					showDeviceIdError();
				}else{
					sendMsg(msgs.get(position).getMessage());
					msgPreDialog.cancel();										
				}
			}
		});

		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				msgPreDialog.cancel();
			}
		});

		msgPreDialog.show();
	}

	@Override
	public void finish() {
		super.finish();
	}
	
	@Override
	public void onClickMenu(View view) {
		finish();
	}
	
	
	final private ServiceConnection serviceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			serialPortService = ((SerialPortBinder) service).getService();
			serialPortService.addListener(listener);
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			serialPortService = null;
		}

	};

	private ISerialPortListener listener = new ISerialPortListener() {

		@Override
		public void gotRequestedId(VehicleStatus vehicleStatus) {
			processVehicleStatus(vehicleStatus);

		}

		@Override
		public void gotMessage(Message message) {
			processMessage(message);

		}


		@Override
		public void gotEvento(TrigeredEvent evento) {
			processEvento(evento);

		}

		@Override
		public void gotRequestedIds(String[] ids) {
			gotIds(ids);
			
		}

		@Override
		public void showStatus(String status) {
			
		}

		@Override
		public void showRaw(String response) {
			// TODO Auto-generated method stub
			
		}

	};
	
	protected void processVehicleStatus(VehicleStatus vehicleStatus) {
		// TODO Auto-generated method stub

	}

	protected void gotIds(String[] ids) {
		// TODO Auto-generated method stub
		
	}

	protected void processEvento(final TrigeredEvent evento) {
		switch (evento.getEvento().getAlertaTipo()) {
		case AUDIO:
			
			NotificationUtil.notifyEvento(this, evento);
			break;
		case TEXTO:
					startActivityForResult(new Intent(BusChatActivity.this,EventNotificationActivity.class),ConstUtil.EVENTO_NOTIFICATION_REQUEST);
			break;
		}
	}

	protected void processMessage(Message message) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					refresh();
				}
			});
			
	}

	@Override
	public void onBackPressed() {
		//Do Nothing
	}

	protected MessageDAO messageDAO() {
		if(dao == null) dao = new MessageDAO(this);

		return dao;
	}

	protected void finishRout(){
		serialPortService.finishRout();
	}

	protected void sendMessage(Message message){
		serialPortService.sendMessage(message);
	}

	protected void sendRawMessage(String rawMessage){
		serialPortService.sendRawMessage(rawMessage);
	}
	
	protected void sendOpenRotaCommand(Rota rota){
		serialPortService.openRout(rota);
	}
}
