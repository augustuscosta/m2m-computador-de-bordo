package com.m2msolutions.m2mcomputadordebordo.activity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.KeyEvent;
import android.view.Menu;
import android.widget.Toast;

import com.m2msolutions.m2mcomputadordebordo.database.MessageDAO;
import com.m2msolutions.m2mcomputadordebordo.model.Message;
import com.m2msolutions.m2mcomputadordebordo.model.MessageOwner;
import com.m2msolutions.m2mcomputadordebordo.model.TrigeredEvent;
import com.m2msolutions.m2mcomputadordebordo.model.VehicleStatus;
import com.m2msolutions.m2mcomputadordebordo.service.SerialPortService;
import com.m2msolutions.m2mcomputadordebordo.service.SerialPortService.ISerialPortListener;
import com.m2msolutions.m2mcomputadordebordo.service.SerialPortService.ISerialPortService;
import com.m2msolutions.m2mcomputadordebordo.service.SerialPortService.SerialPortBinder;
import com.m2msolutions.m2mcomputadordebordo.util.ConstUtil;
import com.m2msolutions.m2mcomputadordebordo.util.NotificationUtil;

public class GenericSerialPortActivity extends GenericActivity {

	protected ISerialPortService serialPortService;

	// Serial port stuff

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onResume() {
		bindService(new Intent(SerialPortService.SERIAL_PORT_SERVICE_INTENT), serviceConnection, BIND_AUTO_CREATE);
		super.onResume();
	}


	@Override
	protected void onPause() {
		unbindSerialPortService();
		super.onPause();
	}

	private void unbindSerialPortService() {
		if(serialPortService != null) 	serialPortService.removeListener(listener);
		unbindService(serviceConnection);
		serialPortService = null;
	}

	// KIOSKE MODE THINGS

	@Override
	public boolean onSearchRequested() {
		return false;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (acceptKey(keyCode)) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onKeyLongPress(int keyCode, KeyEvent event) {
		if (acceptKey(keyCode)) {
			return true;
		}

		return super.onKeyLongPress(keyCode, event);
	}

	public static boolean acceptKey(final int keyCode) {
		if ((KeyEvent.KEYCODE_HOME == keyCode || KeyEvent.KEYCODE_SEARCH == keyCode)) {

			return true;
		}

		return false;
	}

	// MENU SECURITY

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return showMenuSecurity();
	}

	protected boolean showMenuSecurity() {
		Intent intent = new Intent(GenericSerialPortActivity.this,
				AutenticacaoActivity.class);
		startActivityForResult(intent, ConstUtil.SECURITY_REQUEST);
		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {

		super.onActivityResult(requestCode, resultCode, intent);
	}


	final private ServiceConnection serviceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			serialPortService = ((SerialPortBinder) service).getService();
			serialPortService.addListener(listener);
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			serialPortService = null;
		}

	};

	private ISerialPortListener listener = new ISerialPortListener() {

		@Override
		public void gotRequestedId(VehicleStatus vehicleStatus) {
			processVehicleStatus(vehicleStatus);

		}

		@Override
		public void gotMessage(Message message) {
			processMessage(message);

		}


		@Override
		public void gotEvento(TrigeredEvent evento) {
			processEvento(evento);
		}

		@Override
		public void gotRequestedIds(String[] ids) {
			gotIds(ids);
			
		}

		@Override
		public void showStatus(String status) {
			if(status != null) showStatusFromService(status);
			
		}

		@Override
		public void showRaw(String response) {
			showRawMessage(response);
			
		}

	};
	
	
	protected void showRawMessage(String response){
		
	}

	private MessageDAO messageDAO;

	protected void processEvento(final TrigeredEvent evento) {
		switch (evento.getEvento().getAlertaTipo()) {
		case AUDIO:

			NotificationUtil.notifyEvento(this, evento);
			break;

		case TEXTO:
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					startActivityIfNeeded(new Intent(GenericSerialPortActivity.this,EventNotificationActivity.class),Intent.FLAG_ACTIVITY_SINGLE_TOP);
				}
			});
			break;
		}
	}

	protected void showStatusFromService(final String status) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(GenericSerialPortActivity.this, status, Toast.LENGTH_SHORT).show();
			}
		});		
	}
		

	protected void gotIds(String[] ids) {
		// TODO Auto-generated method stub
	}

	protected void processVehicleStatus(VehicleStatus vehicleStatus) {
		// TODO Auto-generated method stub
	}

	protected void processMessage(final Message message) {
		if(message.getMessageOwner() == MessageOwner.MOTORISTA){
			return;
		}
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				startActivityIfNeeded(new Intent(GenericSerialPortActivity.this,MessageNotificationActivity.class),Intent.FLAG_ACTIVITY_SINGLE_TOP);
			}
		});		
	}

	@Override
	public void onBackPressed() {
		//Do Nothing
	}


	protected MessageDAO messageDAO() {
		if(messageDAO == null) messageDAO = new MessageDAO(this);

		return messageDAO;
	}

	protected void finishRout() {
		serialPortService.finishRout();
	}

	public void sendRawMessage(String rawMessage) {
		serialPortService.sendRawMessage(rawMessage);

	}

	protected void sendMessage(Message message){
		serialPortService.sendMessage(message);
	}
	
	public void setRaw(Boolean raw){
		serialPortService.setRaw(raw);
	}
	
	public void sendOffProtocol(String message){
		serialPortService.sendOffProtocol(message);
	}

}
