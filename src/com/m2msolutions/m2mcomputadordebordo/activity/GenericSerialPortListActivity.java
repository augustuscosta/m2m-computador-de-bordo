package com.m2msolutions.m2mcomputadordebordo.activity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.KeyEvent;
import android.view.Menu;
import android.widget.Toast;

import com.m2msolutions.m2mcomputadordebordo.database.MessageDAO;
import com.m2msolutions.m2mcomputadordebordo.model.Message;
import com.m2msolutions.m2mcomputadordebordo.model.MessageOwner;
import com.m2msolutions.m2mcomputadordebordo.model.Rota;
import com.m2msolutions.m2mcomputadordebordo.model.TrigeredEvent;
import com.m2msolutions.m2mcomputadordebordo.model.VehicleStatus;
import com.m2msolutions.m2mcomputadordebordo.service.SerialPortService;
import com.m2msolutions.m2mcomputadordebordo.service.SerialPortService.ISerialPortListener;
import com.m2msolutions.m2mcomputadordebordo.service.SerialPortService.ISerialPortService;
import com.m2msolutions.m2mcomputadordebordo.service.SerialPortService.SerialPortBinder;
import com.m2msolutions.m2mcomputadordebordo.util.ConstUtil;
import com.m2msolutions.m2mcomputadordebordo.util.NotificationUtil;

public class GenericSerialPortListActivity extends GenericListActivity{

	private ISerialPortService serialPortService;
	private MessageDAO messageDAO;

	// Serial port stuff

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onResume() {
		bindService(new Intent(SerialPortService.SERIAL_PORT_SERVICE_INTENT), serviceConnection, BIND_AUTO_CREATE);	
		super.onResume();
	}

	@Override
	protected void onPause() {
		unbindSerialPortService();
		super.onPause();
	}

	private void unbindSerialPortService() {
		if(serialPortService != null) serialPortService.removeListener(listener);
		unbindService(serviceConnection);
		serialPortService = null;
	}

	// KIOSKE MODE THINGS

	@Override
	public boolean onSearchRequested() {
		return false;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (acceptKey(keyCode)) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onKeyLongPress(int keyCode, KeyEvent event) {
		if (acceptKey(keyCode)) {
			return true;
		}

		return super.onKeyLongPress(keyCode, event);
	}

	public static boolean acceptKey(final int keyCode) {
		if ((KeyEvent.KEYCODE_HOME == keyCode || KeyEvent.KEYCODE_SEARCH == keyCode)) {

			return true;
		}

		return false;
	}

	// MENU SECURITY

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return showMenuSecurity();
	}

	protected boolean showMenuSecurity() {
		Intent intent = new Intent(GenericSerialPortListActivity.this,
				AutenticacaoActivity.class);
		startActivityForResult(intent, ConstUtil.SECURITY_REQUEST);
		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);
	}

	protected void onDataReceived(byte[] buffer, int size) {
		// TODO Auto-generated method stub

	}

	final private ServiceConnection serviceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			serialPortService = ((SerialPortBinder) service).getService();
			serialPortService.addListener(listener);
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			serialPortService = null;
		}

	};

	private ISerialPortListener listener = new ISerialPortListener() {

		@Override
		public void gotRequestedId(VehicleStatus vehicleStatus) {
			processVehicleStatus(vehicleStatus);

		}

		@Override
		public void gotMessage(Message message) {
			processMessage(message);

		}


		@Override
		public void gotEvento(TrigeredEvent evento) {
			processEvento(evento);

		}

		@Override
		public void gotRequestedIds(String[] ids) {
			gotIds(ids);
			
		}

		@Override
		public void showStatus(String status) {
			if(status != null) showStatusFromService(status);
			
		}

		@Override
		public void showRaw(String response) {
			// TODO Auto-generated method stub
			
		}

	};
	
	protected void showStatusFromService(final String status) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(GenericSerialPortListActivity.this, status, Toast.LENGTH_LONG).show();
			}
		});		
	}


	protected void processVehicleStatus(VehicleStatus vehicleStatus) {
		// TODO Auto-generated method stub

	}

	protected void gotIds(String[] ids) {
		// TODO Auto-generated method stub
		
	}

	protected void processEvento(final TrigeredEvent evento) {
		switch (evento.getEvento().getAlertaTipo()) {
		case AUDIO:
			
			NotificationUtil.notifyEvento(this, evento);
			break;
		case TEXTO:
					startActivityForResult(new Intent(GenericSerialPortListActivity.this,EventNotificationActivity.class),ConstUtil.EVENTO_NOTIFICATION_REQUEST);
			break;
		}
	}


	protected void processMessage(final Message message) {
		if(message.getMessageOwner() == MessageOwner.MOTORISTA){
			return;
		}
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				startActivityForResult(new Intent(GenericSerialPortListActivity.this,MessageNotificationActivity.class),
						ConstUtil.MESSAGE_NOTIFICATION_REQUEST);
			}
		});		
	}

	@Override
	public void onBackPressed() {
		//Do Nothing
	}

	protected MessageDAO messageDAO() {
		if(messageDAO == null) messageDAO = new MessageDAO(this);

		return messageDAO;
	}

	protected void finishRout(){
		serialPortService.finishRout();
	}


	protected void sendMessage(Message message){
		serialPortService.sendMessage(message);
	}

	protected void sendRawMessage(String rawMessage){
		serialPortService.sendRawMessage(rawMessage);
	}
	
	protected void sendOpenRotaCommand(Rota rota){
		serialPortService.openRout(rota);
	}

}
