package com.m2msolutions.m2mcomputadordebordo.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.m2msolutions.m2mcomputadordebordo.R;

public class SerialActivityListener extends GenericSerialPortActivity {

	private EditText editTextReception;
	private EditText editTextEmission;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.serial_activity_listener);
		configureActivity();
	}
	
	
	
	@Override
	protected void onPause() {
		setRaw(false);
		super.onPause();
	}

	private void configureActivity() {
		editTextReception = (EditText) findViewById(R.id.editTextReception);
		editTextEmission  = (EditText) findViewById(R.id.EditTextEmission);
	}

	private void conectService() {
		setRaw(true);
	}

	@Override
	protected void showRawMessage(final String response) {
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				String early = editTextReception.getText().toString();
				editTextReception.setText(early+response);
				
			}
		});
	}
	
	@Override
	public void onBackPressed() {
		finish();
	}

	public void onClickSent(View view){
		sendRawMessage(editTextEmission.getText().toString());
	}

	public void onClickClear(View view){
		editTextReception.setText("");
		editTextEmission.setText("");
	}
	
	public void onClickInitialize(View view){
		conectService();
	}

}
