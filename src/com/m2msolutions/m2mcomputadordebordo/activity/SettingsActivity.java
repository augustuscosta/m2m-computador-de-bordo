
package com.m2msolutions.m2mcomputadordebordo.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.m2msolutions.m2mcomputadordebordo.R;
import com.m2msolutions.m2mcomputadordebordo.sdcardfiles.xml.XMLFileReader;
import com.m2msolutions.m2mcomputadordebordo.service.SerialPortService;
import com.m2msolutions.m2mcomputadordebordo.util.AppHelper;
import com.m2msolutions.m2mcomputadordebordo.util.LogUtil;

public class SettingsActivity extends GenericSerialPortActivity {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);

		final Button buttonSetup = (Button)findViewById(R.id.ButtonSetup);
		buttonSetup.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				startActivity(new Intent(SettingsActivity.this, SerialPortPreferences.class));
			}
		});

		final Button buttonConsole = (Button)findViewById(R.id.ButtonConsole);
		buttonConsole.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				startActivity(new Intent(SettingsActivity.this, SerialActivityListener.class));
			}
		});

		final Button buttonLoopback = (Button)findViewById(R.id.ButtonLoopback);
		buttonLoopback.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				startActivity(new Intent(SettingsActivity.this, ReadSerialActivity.class));
			}
		});

		final Button settingsButton = (Button)findViewById(R.id.ButtonConfiguration);
		settingsButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				startActivity(new Intent(Settings.ACTION_SETTINGS));
			}
		});

		final Button requestIdButton = (Button)findViewById(R.id.RequestIdButton);
		requestIdButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(SettingsActivity.this);
				String path = sp.getString("DEVICE", "");
				int baudrate = Integer.decode(sp.getString("BAUDRATE", "-1"));
				if((path.length() == 0) || (baudrate == -1)){
					AppHelper.getInstance().presentError(SettingsActivity.this, getString(R.string.erro),
							getString(R.string.error_configuration));
				}else{
					Toast.makeText(SettingsActivity.this, "requisitando ids pelo servico", Toast.LENGTH_LONG).show();
					LogUtil.i("requisitando ids pelo servico");
					serialPortService.requestVehicleStatus();
				}
			}
		});
		configureActivity();
	}

	private void configureActivity(){
		fotoMotorista = (ImageView) findViewById(R.id.fotoMotorissta);
		relogio = (TextView) findViewById(R.id.relogio);
		linhaTextView = (TextView) findViewById(R.id.linha);
	}


	public void onClickImportData(View view){
		
		runImportDataAsyncTask();
		if (android.os.Environment.getExternalStorageState()
				.equals(android.os.Environment.MEDIA_MOUNTED)) {
			
			try {
				AppHelper.importAllDataFromSdCard(this);
				XMLFileReader.readAndSaveAll(this);
				AppHelper.getInstance().presentError(this, getString(R.string.sucess),
						getString(R.string.sucess_memory));
			} catch (Exception e) {
				LogUtil.e("erro ao importer dados do sdcars",e);
				AppHelper.getInstance().presentError(this, getString(R.string.erro), 
						getString(R.string.error_data));
			}
			
		}else{
			AppHelper.getInstance().presentError(this, getString(R.string.erro), 
					getString(R.string.no_sdcard));			
		}
	}

	private void runImportDataAsyncTask() {
		// TODO Auto-generated method stub
	}
	
	@Override
	public void onClickMenu(View view) {
		finish();
	}
	
    @Override
    protected void gotIds(String[] ids) {
    	
    	if(ids == null || ids.length < 5) return;
    	
    	final String toShow = ids[0] + ids[1] + ids[2] + ids[3] + ids[4];
    	runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				Toast.makeText(SettingsActivity.this, toShow, Toast.LENGTH_LONG).show();
			}
		});
    }
    
    public void onClickRestartService(View view){
    	SerialPortService.restarService(this);
    }
    
}
