package com.m2msolutions.m2mcomputadordebordo.activity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.m2msolutions.m2mcomputadordebordo.R;
import com.m2msolutions.m2mcomputadordebordo.util.Session;

public class AutenticacaoActivity extends GenericActivity{
	
	private EditText passwordText;
	private TextView passwordInfoText;
	private Button passwordOkBtn;
	
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.autenticacao);
		setEditEnterOnClickListener(getPasswordText());
		setConfirmOnClickListener(getPasswordOkBtn());
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return true;
	}
	
	@Override
	public void onBackPressed() {
		finish();
	}
    
    private EditText getPasswordText(){
    	if(passwordText == null){
    		passwordText = (EditText)findViewById(R.id.passwordText);
    	}
    	return passwordText;
    }
    
    private TextView getPasswordInfoText(){
    	if(passwordInfoText == null){
    		passwordInfoText = (TextView)findViewById(R.id.passwordInfoText);
    	}
    	return passwordInfoText;
    }
    
    private Button getPasswordOkBtn(){
    	if(passwordOkBtn == null){
    		passwordOkBtn = (Button)findViewById(R.id.passwordOkBtn);
    	}
    	return passwordOkBtn;
    }
    
    private void setConfirmOnClickListener(View confirmBtn){
    	confirmBtn.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v){
				if(isValidPassword(getPasswordText().getText().toString())){
					setResult(RESULT_OK);
					finish();
				} else {
					getPasswordText().setText("");
					getPasswordInfoText().setText(
							getString(R.string.wrong_password));
					getPasswordInfoText().postDelayed(new Runnable() {
						public void run() {
							getPasswordInfoText().setText(
									getString(R.string.ask_password));
						}
					}, 1500);
				}
			}
		});
    }
    
    private void setEditEnterOnClickListener(EditText editText){
    	editText.setOnEditorActionListener(new OnEditorActionListener(){
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				getPasswordOkBtn().performClick();
				return false;
			}
		});
    }
    
    public void onClickCancel(View view){
    	setResult(RESULT_CANCELED);
    	finish();
    }
    
    private boolean isValidPassword(String password){
    	if(Session.getMasterPassword(this).equals(password))
    		return true;
    	return false;
    }
}