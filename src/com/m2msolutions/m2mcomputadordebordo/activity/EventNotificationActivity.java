package com.m2msolutions.m2mcomputadordebordo.activity;

import java.util.List;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.m2msolutions.m2mcomputadordebordo.R;
import com.m2msolutions.m2mcomputadordebordo.database.TrigeredEventDao;
import com.m2msolutions.m2mcomputadordebordo.model.TrigeredEvent;

public class EventNotificationActivity extends GenericSerialPortActivity{
	
	private int index = 0;
	private TextView messageBody;
	private List<TrigeredEvent> messages ;
	private TrigeredEventDao trigeredEventDao;
	private TextView eventoTypeImage;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.event_notification);
		configureActivity();
	}

	@Override
	public void onClickMenu(View view) {
		// DO Nothing
	}

	private void configureActivity() {
		messageBody = (TextView) findViewById(R.id.message_body);
		fotoMotorista = (ImageView) findViewById(R.id.fotoMotorissta);
		relogio = (TextView) findViewById(R.id.relogio);
		linhaTextView = (TextView) findViewById(R.id.linha);
		eventoTypeImage = (TextView) findViewById(R.id.eventoTypeImage);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		populate();
	}

	private void populate() {
		messages = trigeredEventDao().getAll();
		if(messages != null && !messages.isEmpty()){
			index = messages.size()-1;
			setMessageOnScreen(index);
		}
	}

	private TrigeredEventDao trigeredEventDao() {
		if(trigeredEventDao == null) trigeredEventDao = new TrigeredEventDao(this);
		
		return trigeredEventDao;
	}

	private void setMessageOnScreen(int index) {
		TrigeredEvent message = messages.get(index);
		if(message != null && message.getEvento() != null){
			messageBody.setText(message.getEvento().getTexto());
			
			switch (message.getEvento().getAlertaTipo()) {
			case TEXTO:
				eventoTypeImage.setBackgroundDrawable(getResources().getDrawable(R.drawable.icone_mensagem_escrita));
				break;
				
			case AUDIO:
				eventoTypeImage.setBackgroundDrawable(getResources().getDrawable(R.drawable.icone_mensagem_audio));
				break;

			default:
				break;
			}
		}
	}
	
	public void onClickPrevious(View view){
		if(index > 0){
			index = index-1;
			setMessageOnScreen(index);
		}
	}
	
	public void onClickNext(View view){
		if(index < messages.size()-1){
			index = index +1;
			setMessageOnScreen(index);
		}
	}
	
	public void onClickOk(View view){
			finish();
		}
	
	@Override
	protected void processEvento(TrigeredEvent evento) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				populate();
			}
		});
	}
	
	@Override
	protected void mensagemRecebida() {
		//Do Nothing	
	}
	
	@Override
	protected void eventoRecebido() {
		// DO nothing
	}
	
	@Override
	protected void showStatusFromService(String status) {
		// DO NOTHING
	}

}
