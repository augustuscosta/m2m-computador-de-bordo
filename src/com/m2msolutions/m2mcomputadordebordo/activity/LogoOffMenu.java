package com.m2msolutions.m2mcomputadordebordo.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.m2msolutions.m2mcomputadordebordo.R;
import com.m2msolutions.m2mcomputadordebordo.util.ConstUtil;
import com.m2msolutions.m2mcomputadordebordo.util.Session;

public class LogoOffMenu extends GenericSerialPortActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.log_off_menu);
		configureActivity();
	}
	
	private void configureActivity(){
		fotoMotorista = (ImageView) findViewById(R.id.fotoMotorissta);
		relogio = (TextView) findViewById(R.id.relogio);
		linhaTextView = (TextView) findViewById(R.id.linha);
	}
	
	public void onClickSwitchDriver(View view){
		Session.switchDriver(this);
		setResult(ConstUtil.LOG_OFF_REQUEST);
		finish();
	}
	
	@Override
	public void onClickLogOff(View view){
		finish();
	}
	
	public void onClickCancel(View view){
		finish();
	}
	
	public void onClickCleanSession(View view){
		if(serialPortService!= null){
			finishRout();
		}
		
		Session.logOff(this);
		setResult(ConstUtil.LOG_OFF_REQUEST);
		finish();
		
	}
	
	@Override
	public void onClickMenu(View view) {
		finish();		
	}
	
}
