package com.m2msolutions.m2mcomputadordebordo.activity;

import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.m2msolutions.m2mcomputadordebordo.R;
import com.m2msolutions.m2mcomputadordebordo.adapter.BusServiceAdapter;
import com.m2msolutions.m2mcomputadordebordo.database.RotaDAO;
import com.m2msolutions.m2mcomputadordebordo.model.Rota;
import com.m2msolutions.m2mcomputadordebordo.util.Session;

public class BusServiceListActivity extends GenericSerialPortListActivity {

	private List<Rota> rotas;
	private RotaDAO rotaDAO;
	private EditText lineSearch;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bus_service_list);
		configureActivity();
		populate();
	}

	private void configureActivity() {
		fotoMotorista = (ImageView) findViewById(R.id.fotoMotorissta);
		relogio = (TextView) findViewById(R.id.relogio);
		linhaTextView = (TextView) findViewById(R.id.linha);
		lineSearch = (EditText) findViewById(R.id.lineSearch);
		lineSearch.addTextChangedListener(rotaTextWatcher);
	}


	private void populate() {
		rotas = rotaDAO().getRotasList();
		setAdapter();
	}

	private void setAdapter() {
		BusServiceAdapter adapter = new BusServiceAdapter(this, rotas);
		setListAdapter(adapter);
		this.getListView().setVisibility(View.VISIBLE);
	}

	private void showDeviceIdError() {
		new AlertDialog.Builder(this)
		.setIcon(android.R.drawable.ic_dialog_info)
		.setTitle(R.string.erro)
		.setMessage(R.string.device_id_not_found)
		.setCancelable(false)
		.setNegativeButton(R.string.OK,
				new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog,
					int which) {
				finish();
			}
		}).show();
	}

	private void showRouteUnifinishedError() {
		new AlertDialog.Builder(this)
		.setIcon(android.R.drawable.ic_dialog_info)
		.setTitle(R.string.finish_route)
		.setMessage(R.string.route_unfinish)
		.setCancelable(false)
		.setNegativeButton(R.string.OK,
				new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog,
					int which) {
				dialog.cancel();
			}
		}).show();
	}

	private void showRouteFinish(final Rota rota) {
		new AlertDialog.Builder(this)
		.setIcon(android.R.drawable.ic_dialog_info)
		.setTitle(R.string.alert)
		.setMessage(R.string.finish_route)
		.setCancelable(true)
		.setNegativeButton(R.string.OK,
				new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog,
					int which) {
				finalizarRota(rota);
			}

		}).setPositiveButton(R.string.cancel, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog,
					int which) {
				dialog.cancel();
			}
		}).show();
	}

	private void finalizarRota(final Rota rota) {
		finishRout();
		rota.setStartedAt(null);
		rota.setEmCurso(false);
		rotaDAO().persist(rota);
		setRouteTittle();
		Session.setRouteId(null, BusServiceListActivity.this);
		Session.setRouteName(null, BusServiceListActivity.this);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		if(Session.getDeviceId(this) == null){
			showDeviceIdError();			
		} else {
			Rota rota = rotas.get(position);
			if((Session.getRouteId(this) == null)){
				sendSelectedRouteCommand(rota);
			}
			else{
				if((long)Session.getRouteId(this) != rota.getId()){
					showRouteUnifinishedError();
				}else{
					if(rota.getId() != null && (long)rota.getId() == Session.getRouteId(this)){
						showRouteFinish(rota);
					}
				}
			}
		}
	}

	private void sendSelectedRouteCommand(Rota rota) {
		sendOpenRotaCommand(rota);
		finish();
	}

	@Override
	public void onClickLine(View view) {
		//DO NOTHING
	}

	private RotaDAO rotaDAO() {
		if(rotaDAO == null){
			rotaDAO = new RotaDAO(this);
		}

		return rotaDAO;
	}

	public void onClickSearch(View view){
		rotas = rotaDAO().getRotasList(lineSearch.getText().toString());
		populate();		
	}

	final private TextWatcher rotaTextWatcher = new TextWatcher() {

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			rotas = rotaDAO().getRotasList(s.toString());
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// Do Noting
		}

		@Override
		public void afterTextChanged(Editable s) {
			setAdapter();
		}
	};

	public void  onClickFinishRoute(View view){
		if(Session.getRouteId(this) != null){
			Rota rota = rotaDAO().findRota(Session.getRouteId(this));
			if(rota != null){
				showRouteFinish(rota);
			}
		}
	}

	public void onClickClean(View view){
		lineSearch.setText("");
	}


	@Override
	public void onClickMenu(View view) {
		finish();
	}
	
	@Override
	protected void showStatusFromService(String status) {
		// DO NOTHING
	}
}
