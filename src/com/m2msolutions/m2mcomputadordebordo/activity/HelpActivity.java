package com.m2msolutions.m2mcomputadordebordo.activity;

import com.m2msolutions.m2mcomputadordebordo.R;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class HelpActivity extends GenericActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.help);
		configureActivity();
	}
	
	private void configureActivity() {
		fotoMotorista = (ImageView) findViewById(R.id.fotoMotorissta);
		relogio = (TextView) findViewById(R.id.relogio);
		linhaTextView = (TextView) findViewById(R.id.linha);
	}
	
	@Override
	public void onClickMenu(View view) {
		finish();
	}

}
