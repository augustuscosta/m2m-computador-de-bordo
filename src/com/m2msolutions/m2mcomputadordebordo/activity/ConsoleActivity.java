
package com.m2msolutions.m2mcomputadordebordo.activity;

import java.io.IOException;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.m2msolutions.m2mcomputadordebordo.R;
import com.m2msolutions.m2mcomputadordebordo.protocol.MaxtrackProtocol;

public class ConsoleActivity extends SerialPortActivity {

	EditText mReception;
	EditText Emission;
	final MaxtrackProtocol protocol = new MaxtrackProtocol();
	String data = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.console);

//		setTitle("Loopback test");
		mReception = (EditText) findViewById(R.id.EditTextReception);
		Emission = (EditText) findViewById(R.id.EditTextEmission);
		Emission.setText(protocol.getRequestIdCommand());
		
		
		
		final Button button = (Button) findViewById(R.id.ConfirmButton);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				int i;
		 		CharSequence t = Emission.getText().toString();   
		//		String str = v.getText().toString();  
		 		char[] text = new char[t.length()];
		 		for (i=0; i<t.length(); i++) {
		 			text[i] = t.charAt(i);
		 		}
				try {
		 			mOutputStream.write(protocol.hexStringToByteArray(new String(text)));
		 			Toast.makeText(ConsoleActivity.this, String.valueOf(text),  Toast.LENGTH_LONG).show();
		//			v.setText("");
				} catch (IOException e) {
					Toast.makeText(ConsoleActivity.this, "Erro escrevendo o comando.",  Toast.LENGTH_LONG).show();
				}
			}
		});
		
		final Button readButton = (Button) findViewById(R.id.ReadButton);
		readButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				mReception.setText(data);
			}
		});
	}

	@Override
	protected void onDataReceived(final byte[] buffer, final int size) {
		data = new String(buffer);
	}
}
