package com.m2msolutions.m2mcomputadordebordo.activity;

import java.util.List;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.m2msolutions.m2mcomputadordebordo.R;
import com.m2msolutions.m2mcomputadordebordo.adapter.EventoAdapter;
import com.m2msolutions.m2mcomputadordebordo.database.TrigeredEventDao;
import com.m2msolutions.m2mcomputadordebordo.model.TrigeredEvent;

public class EventoListActivity extends GenericSerialPortListActivity {

	private List<TrigeredEvent> eventos;
	private EventoAdapter adapter;
	private TrigeredEventDao eventoDao;
	private TextView textView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.event_list);
		configureActivity();
	}

	private void configureActivity() {
		fotoMotorista = (ImageView) findViewById(R.id.fotoMotorissta);
		relogio = (TextView) findViewById(R.id.relogio);
		linhaTextView = (TextView) findViewById(R.id.linha);
		textView = (TextView) findViewById(R.id.textNotification);
	}

	private void refresh() {
		eventos = eventoDao().getAll();
		if(eventos != null && !eventos.isEmpty()){
			textView.setVisibility(View.GONE);
		}else{
			textView.setVisibility(View.VISIBLE);	
		}
		setAdapter();
	}

	private TrigeredEventDao eventoDao() {
		if(eventoDao == null) eventoDao = new TrigeredEventDao(this);

		return eventoDao;
	}

	private void setAdapter(){
		adapter = new EventoAdapter(this, eventos);
		setListAdapter(adapter);
	}

	@Override
	protected void onResume() {
		super.onResume();
		refresh();
	}


	@Override
	protected void processEvento(TrigeredEvent evento) {
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				refresh();
				
			}
		});
		super.processEvento(evento);
	}
	
	@Override
	public void onClickMenu(View view) {
		finish();
	}
	
}
