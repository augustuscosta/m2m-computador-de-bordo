package com.m2msolutions.m2mcomputadordebordo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.m2msolutions.m2mcomputadordebordo.R;
import com.m2msolutions.m2mcomputadordebordo.util.ConstUtil;
import com.m2msolutions.m2mcomputadordebordo.util.Session;

public class MenuActivity extends GenericActivity{
	
	
	private LinearLayout lowButtonContair;
	private LinearLayout upButtonContair;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.transparent_menu);
		configureActivity();
		setRouteTittle();
		loadMotoristaPic();
		updateTime();
	}
	
	@Override
	protected void onResume() {
		showButtons();
		super.onResume();
		if(Session.getDriverId(this) == null) finish();
	}

	private void showButtons() {
		lowButtonContair.setVisibility(View.VISIBLE);
		upButtonContair.setVisibility(View.VISIBLE);
	}
	
	private void configureActivity() {
		fotoMotorista = (ImageView) findViewById(R.id.fotoMotorissta);
		relogio = (TextView) findViewById(R.id.relogio);
		linhaTextView = (TextView) findViewById(R.id.linha);
		upButtonContair = (LinearLayout) findViewById(R.id.upButtonContair);
		lowButtonContair = (LinearLayout) findViewById(R.id.lowButtonContair);
	}
	
	@Override
	protected void onPause() {
		hideButtons();
		super.onPause();
	}

	private void hideButtons() {
		lowButtonContair.setVisibility(View.GONE);
		upButtonContair.setVisibility(View.GONE);
	}

	public void onClickMenu(View view){
		//Do Nothning
	}
	
	public void onClickLogOff(View view){
		startActivityForResult(new Intent(this,LogoOffMenu.class), ConstUtil.LOG_OFF_REQUEST);
	}
	
	public void onClickHelp(View view) {
		startActivityForResult(new Intent(this,HelpActivity.class), ConstUtil.HELP_REQUEST);
	}
	
	public void onClickSearchRoute(View view) {
		startActivityForResult(new Intent(MenuActivity.this, BusServiceListActivity.class),ConstUtil.BUS_SERVICE);
	}
	
	public void onClickOpenBusChat(View view) {
		startActivityForResult(new Intent(MenuActivity.this, BusChatActivity.class),ConstUtil.BUS_CHAT);
	}
	
	public void onClickOpenEventList(View view) {
		startActivityForResult(new Intent(this, EventoListActivity.class), ConstUtil.EVENT_REQUEST);
	}
	
	public void onClickOpenSettings(View view) {
		startActivityForResult(new Intent(MenuActivity.this, AutenticacaoActivity.class), ConstUtil.SECURITY_REQUEST);
	}
	
	public void onClickOpenSinotico(View view) {
		startActivityForResult(new Intent(MenuActivity.this, SinoticoActivity.class), ConstUtil.SINOTICO_REQUEST);
	}
	
	

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
}
