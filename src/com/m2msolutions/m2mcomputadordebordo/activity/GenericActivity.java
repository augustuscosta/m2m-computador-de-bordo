 package com.m2msolutions.m2mcomputadordebordo.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.BitmapDrawable;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.m2msolutions.m2mcomputadordebordo.sdcardfiles.soundsandpics.SoundAndPicturesTransfer;
import com.m2msolutions.m2mcomputadordebordo.service.SerialPortServiceDelegate;
import com.m2msolutions.m2mcomputadordebordo.util.AppHelper;
import com.m2msolutions.m2mcomputadordebordo.util.ConstUtil;
import com.m2msolutions.m2mcomputadordebordo.util.Session;


public abstract class GenericActivity extends Activity {

	protected ImageView fotoMotorista;
	protected TextView relogio;
	protected TextView linhaTextView;

	@Override
	protected void onResume() {
		super.onResume();
		registerReceiver(timeBroadCastReceiver, new IntentFilter(Intent.ACTION_TIME_TICK));
		registerReceiver(messageBroadCastReceiver, new IntentFilter(SerialPortServiceDelegate.MESSAGEM_RECEBIDA));
		registerReceiver(eventBroadCastReceiver, new IntentFilter(SerialPortServiceDelegate.EVENTO_RECEBIDO));
		updateTime();
		loadMotoristaPic();
		setRouteTittle();
	}

	@Override
	protected void onPause() {
		unregisterReceiver(timeBroadCastReceiver);
		unregisterReceiver(messageBroadCastReceiver);
		unregisterReceiver(eventBroadCastReceiver); 
		super.onPause();
	}

	// KIOSKE MODE THINGS

	@Override
	public boolean onSearchRequested() {
		return false;
	}

	@Override
	public boolean onKeyLongPress(int keyCode, KeyEvent event) {
		if ( AppHelper.acceptKey(keyCode) && AppHelper.acceptKeyKioskMode(keyCode)&& AppHelper.acceptKeyKioskModePower(keyCode) ) {
			return true;
		}

		return super.onKeyLongPress(keyCode, event);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ( AppHelper.acceptKey(keyCode) && AppHelper.acceptKeyKioskMode(keyCode)&& AppHelper.acceptKeyKioskModePower(keyCode) ) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if ( AppHelper.acceptKey(keyCode) && AppHelper.acceptKeyKioskMode(keyCode)&& AppHelper.acceptKeyKioskModePower(keyCode) ) {
			return true;
		}

		return super.onKeyUp(keyCode, event);
	}

	@Override
	public void onBackPressed() {
		// DO Noting
	}


	// MENU SECURITY

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return true;
	}

	protected boolean showMenuSecurity() {
		Intent intent = new Intent(GenericActivity.this,
				AutenticacaoActivity.class);
		startActivityForResult(intent, ConstUtil.SECURITY_REQUEST);
		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		if (requestCode == ConstUtil.SECURITY_REQUEST
				&& resultCode == RESULT_OK) {
			startActivity(new Intent(GenericActivity.this,
					SettingsActivity.class));
		}

		if (resultCode == ConstUtil.LOG_OFF_REQUEST) {
			setResult(ConstUtil.LOG_OFF_REQUEST);
			finish();
		}

	}

	//Time update sequence

	final private BroadcastReceiver timeBroadCastReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			updateTime();
		}
	};

	protected void updateTime() {
		if(relogio != null) relogio.setText(AppHelper.getInstance().getFormatedTime());
	}

	//Message Reciever
	final private BroadcastReceiver messageBroadCastReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			mensagemRecebida();
		}
	};
	
	final private BroadcastReceiver eventBroadCastReceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			eventoRecebido();
		}

	};

	protected void eventoRecebido() {
		Intent intent = new Intent(this,  EventNotificationActivity.class);
		startActivityIfNeeded(intent, Intent.FLAG_ACTIVITY_SINGLE_TOP);
	}

	//Header settings
	protected void loadMotoristaPic() {
		BitmapDrawable bitmapDrawable =SoundAndPicturesTransfer.loadMotoristaPic(this);
		if(bitmapDrawable != null && fotoMotorista != null)	fotoMotorista.setBackgroundDrawable(bitmapDrawable);
	}

	protected void mensagemRecebida() {
		Intent intent = new Intent(this, MessageNotificationActivity.class);
		startActivityIfNeeded(intent, Intent.FLAG_ACTIVITY_SINGLE_TOP);
	}

	protected void setRouteTittle(){
		if(Session.getRouteId(this) != null  && linhaTextView != null){
			linhaTextView.setText(Session.getRouteName(this));
		}else{
			if(linhaTextView != null)	linhaTextView.setText("");				
		}
	}

	public void onClickMenu(View view){
		startActivityForResult(new Intent(this, MenuActivity.class ),ConstUtil.MENU_REQUEST);
	}

	public void onClickLogOff(View view){
		startActivityForResult(new Intent(this,LogoOffMenu.class), ConstUtil.LOG_OFF_REQUEST);
	}

	public void onClickLine(View view){
		startActivityForResult(new Intent(this,BusServiceListActivity.class), ConstUtil.BUS_SERVICE);				
	}

}
