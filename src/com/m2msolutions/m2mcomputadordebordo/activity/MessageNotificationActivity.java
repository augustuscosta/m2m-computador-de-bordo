package com.m2msolutions.m2mcomputadordebordo.activity;

import java.util.Date;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.m2msolutions.m2mcomputadordebordo.R;
import com.m2msolutions.m2mcomputadordebordo.model.Message;
import com.m2msolutions.m2mcomputadordebordo.model.MessageOwner;
import com.m2msolutions.m2mcomputadordebordo.util.AppHelper;
import com.m2msolutions.m2mcomputadordebordo.util.ConstUtil;
import com.m2msolutions.m2mcomputadordebordo.util.Session;

public class MessageNotificationActivity extends GenericSerialPortActivity{
	
	private int index = 0;
	private TextView messageBody;
	private List<Message> messages;
	private Button response;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.message_notification);
		configureActivity();
	}

	@Override
	public void onClickMenu(View view) {
		// DO Nothing
	}

	private void configureActivity() {
		messageBody = (TextView) findViewById(R.id.message_body);
		fotoMotorista = (ImageView) findViewById(R.id.fotoMotorissta);
		relogio = (TextView) findViewById(R.id.relogio);
		linhaTextView = (TextView) findViewById(R.id.linha);
		response = (Button) findViewById(R.id.response);
	}
	
	@Override
	protected void onResume() {
		getResponseVisibilty();
		super.onResume();
		populate();
	}

	private void getResponseVisibilty() {
		if(Session.getDriverId(this) == null){
			response.setVisibility(View.GONE);
		}else{
			response.setVisibility(View.VISIBLE);			
		}
	}

	private void populate() {
		messages = messageDAO().getAllFromServer();
		if(messages != null && !messages.isEmpty()){
			index = messages.size()-1;
			setMessageOnScreen(index);
		}
	}

	private void setMessageOnScreen(int index) {
		Message message = messages.get(index);
		if(message != null && message.getMessage() != null){
			messageBody.setText(message.getMessage());
		}
	}
	
	public void onClickPrevious(View view){
		if(index > 0){
			index = index-1;
			setMessageOnScreen(index);
		}
	}
	
	public void onClickNext(View view){
		if(index < messages.size()-1){
			index = index +1;
			setMessageOnScreen(index);
		}
	}
	
	public void onClickOk(View view){
		if(response.getVisibility() == View.GONE){
			finish();
		}else{
			if(Session.getDeviceId(this) == null){
				AppHelper.showDeviceIdError(this);
				finish();
			}
			
			if(serialPortService!= null){
				Message message2 = new Message();
				message2.setDate(new Date(AppHelper.getCurrentTime()));
				message2.setMessage("Ok");
				message2.setMessageOwner(MessageOwner.MOTORISTA);
				serialPortService.sendMessage(message2);
				finish();
			}			
		}
		
	}
	
	public void onClickResponse(View view){
		Intent intent = new Intent(this, BusChatActivity.class);
		intent.putExtra(ConstUtil.ANSWER, true);
		startActivityForResult(intent, ConstUtil.BUS_CHAT);
		finish();
	}
	
	@Override
	protected void processMessage(Message message) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				populate();
			}
		});
	}
	
	@Override
	protected void mensagemRecebida() {
		//Do Nothing	
	}
	
	@Override
	protected void eventoRecebido() {
		// DO nothing
	}
	
	@Override
	protected void showStatusFromService(String status) {
		// DO NOTHING
	}

}
