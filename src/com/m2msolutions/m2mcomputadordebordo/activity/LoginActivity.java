package com.m2msolutions.m2mcomputadordebordo.activity;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.m2msolutions.m2mcomputadordebordo.R;
import com.m2msolutions.m2mcomputadordebordo.database.MotoristaDAO;
import com.m2msolutions.m2mcomputadordebordo.model.Motorista;
import com.m2msolutions.m2mcomputadordebordo.sdcardfiles.soundsandpics.SoundAndPicturesTransfer;
import com.m2msolutions.m2mcomputadordebordo.sdcardfiles.xml.XMLFileReader;
import com.m2msolutions.m2mcomputadordebordo.service.SerialPortService;
import com.m2msolutions.m2mcomputadordebordo.util.AppHelper;
import com.m2msolutions.m2mcomputadordebordo.util.ConstUtil;
import com.m2msolutions.m2mcomputadordebordo.util.LogUtil;
import com.m2msolutions.m2mcomputadordebordo.util.Session;

public class LoginActivity extends GenericActivity {

	private EditText nameText;
	private EditText senhaText;
	private ImageView logoEmpresa;
	private Button abortButton;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		showSplashScreen();
		configureAcrivity();
		importData();
		SerialPortService.startService(this);
		senhaText.setOnEditorActionListener(new OnEditorActionListener() {
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE) {
					logifFieldsAreValid();
					return true;
				}

				return true;
			}
		});
	}

	@Override
	protected void onResume() {
		loadLogo();
		checkSession();
		super.onResume();
	}

	private void checkSession() {
		if(Session.getDriverId(this) != null){
			abortButton.setVisibility(View.GONE);
			startActivityForResult(new Intent(LoginActivity.this, MenuActivity.class),ConstUtil.MENU_REQUEST);						
		}else{
			if(Session.getDriverId(this) == null && Session.getInSession(this)){
				abortButton.setVisibility(View.VISIBLE);
			}else{
				abortButton.setVisibility(View.GONE);
				senhaText.setText("");
				nameText.setText("");

			}
		}
	}

	private void showSplashScreen() {
		startActivityForResult(new Intent(this, SplashScreen.class), ConstUtil.SPLASHSCREEN);

	}

	private void configureAcrivity() {
		nameText = (EditText) findViewById(R.id.username);
		senhaText = (EditText) findViewById(R.id.password);
		logoEmpresa = (ImageView) findViewById(R.id.logoEmpresa);
		abortButton = (Button) findViewById(R.id.abort_button);
	}

	private void loadLogo(){
		BitmapDrawable bitmapDrawable = SoundAndPicturesTransfer.loadLogo(this);
		if(bitmapDrawable != null){
			logoEmpresa.setBackgroundDrawable(bitmapDrawable);
		}
	}

	public void loginButonClick(View view){
		logifFieldsAreValid();
	}

	private void logifFieldsAreValid() {
		if(nameText.getText().length() >0 && senhaText.getText().length() >0){
			doLogin();			
		}else{
			AppHelper.getInstance().presentError(this, getString(R.string.warning), getString(R.string.missing_info));
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {

		if (requestCode == ConstUtil.SECURITY_REQUEST
				&& resultCode == RESULT_OK) {
			startActivity(new Intent(this,
					SettingsActivity.class));
		}

	}

	private void doLogin(){
		Motorista motorista = new Motorista();
		MotoristaDAO motoristaDAO = new MotoristaDAO(this);
		motorista.setNome(nameText.getText().toString());
		motorista.setSenha(senhaText.getText().toString());
		Motorista logged =  motoristaDAO.log(motorista);
		if(logged != null){
			Session.setDriverId(logged.getId(), this);
			Session.setInSession(this, true);
			startActivityForResult(new Intent(LoginActivity.this, MenuActivity.class),ConstUtil.MENU_REQUEST);			
		}else{
			AppHelper.getInstance().presentError(this, getString(R.string.erro), getString(R.string.wrong_password_or_user));
		}
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return showMenuSecurity();
	}

	protected boolean showMenuSecurity() {
		Intent intent = new Intent(this,
				AutenticacaoActivity.class);
		startActivityForResult(intent, ConstUtil.SECURITY_REQUEST);
		return true;
	}

	public void onClickCancel(View view){
		logifFieldsAreValid();
	}

	@Override
	protected void onDestroy() {
		SerialPortService.stopService(this);
		super.onDestroy();
	}

	private void importData(){
		if (android.os.Environment.getExternalStorageState()
				.equals(android.os.Environment.MEDIA_MOUNTED)) {

			try {
				AppHelper.importAllDataFromSdCard(this);
				XMLFileReader.readAndSaveAll(this);
			} catch (Exception e) {
				LogUtil.e("erro ao importer dados do sdcars",e);
			}

		}
	}
}
