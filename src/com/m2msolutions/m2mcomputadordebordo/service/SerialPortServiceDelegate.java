package com.m2msolutions.m2mcomputadordebordo.service;

import android.content.Context;
import android.content.Intent;

import com.m2msolutions.m2mcomputadordebordo.model.Message;
import com.m2msolutions.m2mcomputadordebordo.model.TrigeredEvent;

public class SerialPortServiceDelegate {
	
	public static final String MESSAGEM_RECEBIDA = "MESSAGEM_RECEBIDA";
	public static final String EVENTO_RECEBIDO = "EVENTO_RECEBIDO";
	private Context context; 
	
	public SerialPortServiceDelegate(Context context){
		this.context = context;
	}

	public void sendMessageOrEvent(Message message, TrigeredEvent event){
		String action;
		Intent intent;
		
		if(message != null){
			action = MESSAGEM_RECEBIDA;
			intent = new Intent();
			intent.setAction(action);
			context.sendBroadcast(intent);
			return;
		}else if(event != null){
			action = EVENTO_RECEBIDO;
			intent = new Intent();
			intent.setAction(action);
			context.sendBroadcast(intent);
			return;			
		}
		
	}

}
