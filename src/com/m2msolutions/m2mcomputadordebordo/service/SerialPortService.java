package com.m2msolutions.m2mcomputadordebordo.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.serialport.SerialPort;

import com.m2msolutions.m2mcomputadordebordo.Application;
import com.m2msolutions.m2mcomputadordebordo.R;
import com.m2msolutions.m2mcomputadordebordo.database.EventoDAO;
import com.m2msolutions.m2mcomputadordebordo.database.MessageDAO;
import com.m2msolutions.m2mcomputadordebordo.database.RotaDAO;
import com.m2msolutions.m2mcomputadordebordo.database.SerialMessageDAO;
import com.m2msolutions.m2mcomputadordebordo.database.TrigeredEventDao;
import com.m2msolutions.m2mcomputadordebordo.model.Evento;
import com.m2msolutions.m2mcomputadordebordo.model.Message;
import com.m2msolutions.m2mcomputadordebordo.model.MessageOwner;
import com.m2msolutions.m2mcomputadordebordo.model.Rota;
import com.m2msolutions.m2mcomputadordebordo.model.RotaTransientOperation;
import com.m2msolutions.m2mcomputadordebordo.model.SerialMessage;
import com.m2msolutions.m2mcomputadordebordo.model.TrigeredEvent;
import com.m2msolutions.m2mcomputadordebordo.model.VehicleStatus;
import com.m2msolutions.m2mcomputadordebordo.protocol.MaxtrackProtocol;
import com.m2msolutions.m2mcomputadordebordo.util.AppHelper;
import com.m2msolutions.m2mcomputadordebordo.util.LogUtil;
import com.m2msolutions.m2mcomputadordebordo.util.Session;

public class SerialPortService extends Service {

	private static final String ISO_8859_1 = "ISO-8859-1";
	private static SerialPortServiceDelegate serialPortServiceDelegate;
	private final MaxtrackProtocol protocol = new MaxtrackProtocol();
	public static final String SERIAL_PORT_SERVICE_INTENT = "SERIAL_PORT_SERVICE_INTENT";
	private static final String UTF_8 = "UTF-8";
//	private static final String CP1252 = "cp1252";
	private final SerialPortBinder binder = new SerialPortBinder();
	private boolean isRunning = false;
	public final List<ISerialPortListener> listeners = new ArrayList<SerialPortService.ISerialPortListener>();
	protected Application mApplication;
	protected SerialPort mSerialPort;
	protected OutputStream mOutputStream;
	private InputStream mInputStream; 
	private ReadThread mReadThread;
	private MessageDAO messageDAO;
	private TrigeredEventDao trigeredEventDao;
	private EventoDAO eventoDAO;
	private SerialMessageDAO serialMessageDAO;
	String intputBuffer ="";
	private boolean isRaw =false;

	public static void startService(Context context) {
		context.startService(new Intent(SERIAL_PORT_SERVICE_INTENT));
	}

	public static void stopService(Context context) {
		context.stopService(new Intent(SERIAL_PORT_SERVICE_INTENT));
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return binder;
	}

	public interface ISerialPortService {
		public void addListener(ISerialPortListener listener);
		public void removeListener(ISerialPortListener listener);
		public VehicleStatus requestVehicleStatus();
		public void sendRawMessage(String rawMessage);
		public void sendMessage(Message message);
		public void finishRout();
		public void notifyStatus(String status);
		public void reuqestIds();
		public void openRout(Rota rota);
		public void setRaw(Boolean raw);
		public void sendOffProtocol(String message);
	}

	public interface ISerialPortListener{
		public void gotMessage(Message message);
		public void gotRequestedId(VehicleStatus vehicleStatus);
		public void gotEvento(TrigeredEvent evento);
		public void gotRequestedIds(String[] ids);
		public void showStatus(String status);
		public void showRaw(String response);
	}

	private ISerialPortService mService = new ISerialPortService() {

		@Override
		public void removeListener(ISerialPortListener listener) {
			if(listeners.contains(listener)) listeners.remove(listener);
		}

		@Override
		public void notifyStatus(String Status) {

		}

		@Override
		public void addListener(ISerialPortListener listener) {
			listeners.add(listener);
		}

		@Override
		public void sendMessage(Message message) {
			LogUtil.i("mensagem sendo enviada " +message.getMessage());
			String command =""; 
			if(Session.getDeviceId(SerialPortService.this)  == null){
				requestVehicleStatus();
				SerialMessage serialCommand = new SerialMessage();
				serialCommand.setMessage(message);
				serialCommand.setSent(0);
				serialMessageDAO().persist(serialCommand);
				return;
			}else{

				LogUtil.d("HEXA DO COMANDO ENVIADO: " + command);
				try {
					command = protocol.getMessageCommand(Session.getDeviceId(SerialPortService.this),message.getMessage());
					mSerialPort = mApplication.getSerialPort(); 
					mOutputStream = mSerialPort.getOutputStream();
					mOutputStream.write(protocol.hexStringToByteArray(command));
					notifyMessagetoListenersAndPersist(message);
				} catch (Exception e) {
					LogUtil.e("Erro enviando a mensagem: " + command, e);
				}
			}

		}

		@Override
		public void sendRawMessage(String rawMessage) {
			try {
				mSerialPort = mApplication.getSerialPort(); 
				mOutputStream = mSerialPort.getOutputStream();
				LogUtil.d("HEXA DO COMANDO ENVIADO: " + rawMessage);
				mOutputStream.write(protocol.hexStringToByteArray(rawMessage));
			} catch (IOException e) {
				LogUtil.e("Erro enviando a mensagem: " + rawMessage, e);
			}

		}

		@Override
		public void finishRout() {
			String command = "";
			LogUtil.i("Fechando rota");
			if(Session.getDeviceId((SerialPortService.this)) != null && Session.getDeviceId((SerialPortService.this)).length > 0 ){
				command = protocol.getCloseRouteCommand(Session.getDeviceId(SerialPortService.this));				
				LogUtil.i("Comando " + command);
				try {
					if(command != null && command.length() >0){
						mOutputStream.write(protocol.hexStringToByteArray(command));					
					}
					Session.setRouteName(null, SerialPortService.this);
					Session.setRouteId(-1, SerialPortService.this);
				} catch (IOException e) {
					LogUtil.e("Erro ao escrever o comando requisitando os ids", e);
				}
			}else{
				SerialMessage serialCommand = new SerialMessage();
				serialCommand.setSent(0);
				RotaTransientOperation rto = new RotaTransientOperation();
				rto.setInitOrFinish(false);
				serialCommand.setrTO(rto);
				serialMessageDAO().persist(serialCommand);
				requestVehicleStatus();
			}
		}

		@Override
		public VehicleStatus requestVehicleStatus() {
			notifyStatusToListeners(getString(R.string.try_conected));
			Session.setDeviceId(null, SerialPortService.this);
			MaxtrackProtocol protocol = new MaxtrackProtocol();
			isRunning = true;
			LogUtil.i("Requisitando IDs");
			String command = protocol.getRequestIdCommand();
			LogUtil.i("Comando " + command);
			try {
				mOutputStream.write(protocol.hexStringToByteArray(command));
			} catch (IOException e) {
				LogUtil.e("Erro ao escrever o comando requisitando os ids", e);
			}
			return null;
		}

		@Override
		public void reuqestIds() {
			requestVehicleStatus();
		}

		@Override
		public void openRout(Rota rota) {
			if(Session.getDeviceId((SerialPortService.this)) != null && Session.getDeviceId((SerialPortService.this)).length > 0 ){
				final String command = protocol.getOpenRouteCommand(Session.getDeviceId(SerialPortService.this),(int)((long)rota.getId()));
				LogUtil.i("Mensagem de selcionar rota "+command);
				try {
					mOutputStream.write(protocol.hexStringToByteArray(command));
				} catch (IOException e) {
					LogUtil.e("Erro ao escrever o comando requisitando os ids", e);
				}
			}else{
				SerialMessage serialCommand = new SerialMessage();
				serialCommand.setSent(0);
				RotaTransientOperation rto = new RotaTransientOperation();
				rto.setInitOrFinish(true);
				rto.setRota(rota);
				serialCommand.setrTO(rto);
				serialMessageDAO().persist(serialCommand);
				requestVehicleStatus();

			}
			Session.setRouteId((int)((long)rota.getId()), SerialPortService.this);
			Session.setRouteName(rota.getNome(), SerialPortService.this);
			rota.setStartedAt(AppHelper.getCurrentDate());
			rota.setEmCurso(true);
			rotaDAO().persist(rota);				

		}

		@Override
		public void setRaw(Boolean raw) {
			LogUtil.i("alternando o estado de leitura entre protocolo e nativo: "+ String.valueOf(raw));
			isRaw = raw;
		}

		@Override
		public void sendOffProtocol(String message) {
			try {
				mSerialPort = mApplication.getSerialPort(); 
				mOutputStream = mSerialPort.getOutputStream();
				LogUtil.d("ENVIADO: " + message);
				if(message.matches("^[0-9A-Fa-f]+$")){
					mOutputStream.write(protocol.hexStringToByteArray(message));
				}else{
					mOutputStream.write(message.getBytes());					
				}
			} catch (IOException e) {
				LogUtil.e("Erro enviando a mensagem: " + message, e);
			}
		}
	};
	private RotaDAO rotaDAO;

	public class SerialPortBinder extends Binder {
		public ISerialPortService getService() {
			return mService;
		}
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Session.clearIds();
		startReadableThread();
		return super.onStartCommand(intent, flags, startId);
	}

	private void startReadableThread() {
		mApplication = (Application) getApplication();
		try {
			mSerialPort = mApplication.getSerialPort(); 
			mOutputStream = mSerialPort.getOutputStream();
			mInputStream = mSerialPort.getInputStream();
			ReadThread mThread = new ReadThread();
			mThread.start();
			mService.requestVehicleStatus();
		} catch (InvalidParameterException e) {
			LogUtil.e("Parametro Invalido || sem porta serial", e);
		} catch (SecurityException e) {
			LogUtil.e("Falha de seguranca", e);
		} catch (IOException e) {
			LogUtil.e("Excessao na entrada e saida de dados", e);
		}
	}

	private class ReadThread extends Thread {

		@Override
		public void run() {
			super.run();
			try{
				mSerialPort = mApplication.getSerialPort(); 
				mOutputStream = mSerialPort.getOutputStream();				
			}catch(Exception e){
				LogUtil.e("Excessao na entrada e saida de dados", e);				
			}
			while(!isInterrupted()) {
				int size;
				try {
					isRunning = true;
					byte[] buffer = new byte[8];
					if (mInputStream == null) return;
					size = mInputStream.read(buffer);
					if (size > 0) {
						onDataReceived(buffer, size);
					}
				} catch (IOException e) {
					isRunning = false;
					LogUtil.e("erro na thread do serviço da porta serial",e);
					if(!Thread.currentThread().isAlive())
						Thread.currentThread().start();
					return;
				}
			}
		}
		
	}

	public void onDataReceived(byte[] buffer, int size) {
		if(!isRunning)
			return;

		String response = getResponse(buffer);
		LogUtil.d("leitura nativa:"+String.valueOf(isRaw));
		LogUtil.i("HEXA DA MSG QUE CHEGOU: " + response);
		LogUtil.i("BUFFER ATUAL: " + intputBuffer);
		if (response == null || response.length() < 2)
			return;
		
		if(intputBuffer == null){
			intputBuffer = "";
		}

		if(isRaw){
			publicateResponse(response);
			return;
		}
		if (intputBuffer.length() == 0) {
			if ("01".equals(response.substring(0, 2))) {
				LogUtil.i("encontrado inicializador de mensagens");
				intputBuffer = response;
			}else{
				intputBuffer ="";
			}
		} else {
			intputBuffer += response;
		}

		LogUtil.i("BUFFER ATUAL COM A RESPOSTA: " + intputBuffer);
		if(intputBuffer.lastIndexOf("01") == -1){
			return;
		}

		if (intputBuffer.lastIndexOf("04") == -1 ){
			return;
		}
		LogUtil.i("encontrado finalizador de mensagens");


		intputBuffer = intputBuffer.substring(intputBuffer.indexOf("01"), intputBuffer.lastIndexOf("04"));
		intputBuffer += "04";
		
		LogUtil.i("Comando Hexa do protocolo:" +intputBuffer);

		if(intputBuffer.contains("00000000000000000000000") || intputBuffer.contains("ffff")){
			notifyStatusToListeners(getString(R.string.contected_problems));
			LogUtil.i("Comando Hexa do protocolo ruidoso solicitando id: " +intputBuffer);
			mService.requestVehicleStatus();
		}

		if(intputBuffer.length() > 7 && intputBuffer.substring(4, 6).equals("00")){
			LogUtil.e("posicao 2 e 3 da string"+intputBuffer.substring(4, 6));
			LogUtil.i("Comando Hexa do protocolo com id:" +intputBuffer);
			if(protocol.isValid(intputBuffer)){
				LogUtil.i("detetado requested id "+intputBuffer.substring(4, 6));
				notifyStatusToListeners(getString(R.string.conected));
				processRequestedId(intputBuffer);
				intputBuffer = "";
				return;				
			}else{
				return;
			}
		}else if(intputBuffer.length() > 7 &&  intputBuffer.substring(4, 6).equals("1f")){
			intputBuffer ="";
			return;

		}else if(intputBuffer.length() > 7 &&  intputBuffer.substring(4, 6).equalsIgnoreCase("3c")){
			LogUtil.i("Comando Hexa do protocolo com msg ou evento:" +intputBuffer);

			intputBuffer = protocol.getMessageFromResponse(intputBuffer);
			LogUtil.i("mensagem ou evento ");
			if(intputBuffer == null)
				return;
			try {
				intputBuffer = new String(intputBuffer.getBytes(ISO_8859_1),UTF_8);
				LogUtil.e("mensagem decoficada, "+ intputBuffer);
			} catch (UnsupportedEncodingException e) {
				LogUtil.e("decodificar a mensagem", e);
			}
			if (intputBuffer == null || intputBuffer.length() == 0)
				return;

			if(!(intputBuffer.lastIndexOf("#M") == -1) && !(intputBuffer.lastIndexOf("M#") == -1) ){
				String eventStringNumber = intputBuffer.substring(intputBuffer.indexOf("#M")+2, intputBuffer.lastIndexOf("M#"));
				String eventId = "";
				LogUtil.i("id do evento " + eventStringNumber);
				for(char c : eventStringNumber.toCharArray()){
					if(Character.isDigit(c)){
						eventId+=c;
					}
				}
				Long id = null;
				if(eventId.length() > 0){
					id = Long.parseLong(eventId);				
				}
				if (id != null){
					processEvento(id);
					LogUtil.i("id do evento em long" + id);
					intputBuffer = "";
					return;
				}

			}else{
				final Message mensagem = new Message();
				mensagem.setDate(new Date(AppHelper.getCurrentTime()));
				mensagem.setMessage(intputBuffer);
				mensagem.setMessageOwner(MessageOwner.OPERDADOR);
				intputBuffer = "";
				notifyMessagetoListenersAndPersist(mensagem);			
			}

		}else{
			intputBuffer = "";
		}

	}

	private void publicateResponse(String response) {
		if(listeners!= null && !listeners.isEmpty()){
			for (ISerialPortListener listener : listeners) {
				listener.showRaw(response);
			}
		}
	}

	private void processRequestedId(String message) {
		String[] ids = null;
		try {
			ids = protocol.getDeviceIdFromResponse(message);
		} catch (Exception e) {
			LogUtil.e("erro ao fazer o parser do Id",e);
		}
		if(ids != null){
			Session.setDeviceId(ids, this);
			notifyIdsToListeners(ids);
			notifyStatusToListeners(getString(R.string.conected));
			sendPendingMessages();
		}

	}

	private void notifyIdsToListeners(String[] ids) {
		LogUtil.d("------ notificando listeners do ids do rastreador-----------");		
		for (ISerialPortListener listener : listeners) {
			listener.gotRequestedIds(ids);
		}
	}

	private void notifyStatusToListeners(String status){
		for(ISerialPortListener listener : listeners){
			listener.showStatus(status);
		}
	}

	private void sendPendingMessages() {
		List<SerialMessage> serialMesasges = serialMessageDAO().getAllUnsent();
		for(SerialMessage serialMessage : serialMesasges){
			if(serialMessage.getMessage() != null){
				mService.sendMessage(serialMessage.getMessage());
			}else if(serialMessage.getrTO() != null){
				if(serialMessage.getrTO().isInitOrFinish() && serialMessage.getrTO().getRota() != null){
					mService.openRout(serialMessage.getrTO().getRota());
				}else if(!serialMessage.getrTO().isInitOrFinish()){
					mService.finishRout();
				}
			}

			serialMessage.setSent(1);
			try {
				serialMessageDAO().update(serialMessage);
			} catch (Exception e) {
				LogUtil.e("erro ao enviar ao atualizar comando em cast", e);
			}
		}
	}

	private void processEvento(Long id) {
		Evento evento = eventoDAO().find(id);
		TrigeredEvent event = new TrigeredEvent();
		event.setEvento(evento);
		event.setEventoId(evento.getId());
		event.setDate(AppHelper.getCurrentDate());
		notifyAndPersistEvent(event);
	}

	private void notifyAndPersistEvent(TrigeredEvent event) {
		trigeredEventDao().persist(event);
		serialPortServiceDelegate().sendMessageOrEvent(null, event);
		for (ISerialPortListener listener : listeners) {
			listener.gotEvento(event);
		}
	}

	private void notifyMessagetoListenersAndPersist(Message mensagem) {
		LogUtil.d("------ notificando listeners da mensagem-----------"+"  "+mensagem.getMessage());
		messageDAO().persist(mensagem);

		serialPortServiceDelegate().sendMessageOrEvent(mensagem, null);
		for (int i = 0; i < listeners.size(); i++) {

			listeners.get(i).gotMessage(mensagem);
		}
	}

	@Override
	public void onDestroy() {
		if (mReadThread != null){
			mReadThread.interrupt();
		}
		mSerialPort = null;
		super.onDestroy();
	}

	private String getResponse(byte[] buffer) {
		MaxtrackProtocol protocol = new MaxtrackProtocol();
		String toReturn = null;
		try {
			toReturn = protocol.getHexString(buffer);
		} catch (UnsupportedEncodingException e) {
			LogUtil.e("erro no parser de byte[] para hex string");
		}
		return toReturn;
	}

	private MessageDAO messageDAO() {
		if(messageDAO == null) messageDAO = new MessageDAO(this);

		return messageDAO;
	}

	private TrigeredEventDao trigeredEventDao(){
		if(trigeredEventDao == null) trigeredEventDao = new TrigeredEventDao(this);

		return trigeredEventDao;
	}

	private EventoDAO eventoDAO(){
		if (eventoDAO == null)  eventoDAO = new EventoDAO(this);

		return eventoDAO;
	}

	private SerialMessageDAO serialMessageDAO(){
		if (serialMessageDAO == null) serialMessageDAO = new SerialMessageDAO(this);

		return serialMessageDAO;
	}

	private RotaDAO rotaDAO() {
		if(rotaDAO == null){
			rotaDAO = new RotaDAO(this);
		}

		return rotaDAO;
	}

	private  SerialPortServiceDelegate serialPortServiceDelegate(){
		if(serialPortServiceDelegate == null){
			serialPortServiceDelegate = new SerialPortServiceDelegate(this);
		}

		return serialPortServiceDelegate;
	}

	public static void restarService(Context context){
		stopService(context);
		startService(context);
	}

}
