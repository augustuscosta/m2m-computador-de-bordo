package com.m2msolutions.m2mcomputadordebordo.database;

import java.sql.SQLException;
import java.util.List;

import com.m2msolutions.m2mcomputadordebordo.model.MensagemPre;
import com.m2msolutions.m2mcomputadordebordo.util.LogUtil;

import android.content.Context;

public class MensagemPreDao extends DaoProvider {

	public MensagemPreDao(Context context) {
		super(context);
	}

	public List<MensagemPre> getAllFromDataBase(){
		try {
			return getMensagemPreDao().queryForAll();
		} catch (SQLException e) {
			LogUtil.e("erro ao pegar Mensagens pr� do banco de dados", e);
			return null;
		}finally{
			try {
				getMensagemPreDao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro ao fechar cursor das mensagens pr�", e);
			}
		}
	}

	public MensagemPre find(long id){
		try {
			return getMensagemPreDao().queryForId(id);
		} catch (SQLException e) {
			LogUtil.e("erro ao pegar Mensagem pr� do banco de dados" + id, e);
			return null;
		}finally{
			try {
				getMensagemPreDao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro ao fechar cursor das mensagens pr�", e);
			}
		}
	}

	public void persist(List<MensagemPre> mensagensPre){
		if (mensagensPre == null || mensagensPre.isEmpty()) {
			return;
		}

		for (MensagemPre mensagemPre : mensagensPre) {
			persist(mensagemPre);
		}

	}

	private void persist(MensagemPre mensagemPre) {
		if (mensagemPre == null) {
			return;
		}
		try {
			getMensagemPreDao().createOrUpdate(mensagemPre);
		} catch (SQLException e) {
			LogUtil.e("erro ao persisitir Mensagem pr� definida no banco de dados", e);
		}finally{
			try {
				getMensagemPreDao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro ao fechar cursor das mensagens pr�", e);
			}
		}
	}

}
