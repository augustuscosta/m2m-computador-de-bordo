package com.m2msolutions.m2mcomputadordebordo.database;

import java.sql.SQLException;

import android.content.Context;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.m2msolutions.m2mcomputadordebordo.model.Configuracao;
import com.m2msolutions.m2mcomputadordebordo.model.Evento;
import com.m2msolutions.m2mcomputadordebordo.model.MensagemPre;
import com.m2msolutions.m2mcomputadordebordo.model.Message;
import com.m2msolutions.m2mcomputadordebordo.model.Motorista;
import com.m2msolutions.m2mcomputadordebordo.model.Ponto;
import com.m2msolutions.m2mcomputadordebordo.model.Rota;
import com.m2msolutions.m2mcomputadordebordo.model.RotaTransientOperation;
import com.m2msolutions.m2mcomputadordebordo.model.SerialMessage;
import com.m2msolutions.m2mcomputadordebordo.model.TrigeredEvent;
import com.m2msolutions.m2mcomputadordebordo.util.LogUtil;

/**
 * @author bruno.tiger@gmail.com
 * @since 29/11/2012
 * @version $Revision:  $ <br>
 *          $Date:  $ <br> 
 *          $Author:  $
 */

public class DaoProvider{

	private OrmLiteSqliteOpenHelper databaseHelper;
	private Dao<Rota, Long> rotaDao;
	private Dao<Ponto, Long> pontoDao;
	private Dao<Motorista, Long> motoristaDao;
	private Dao<Evento, Long> eventoDao;
	private Dao<MensagemPre, Long> mensagemPreDao;
	private Dao<Message, Long> messageDao;
	private Dao <Configuracao, Long> configuracaoDao;
	private Dao <TrigeredEvent, Long> trigeredEventDao;
	private Dao <SerialMessage, Long> serialMessageDao;
	private Dao<RotaTransientOperation, Long> rTODAO;

	public DaoProvider(Context context){
		this.databaseHelper = DatabaseHelper.getDatabase(context);
	}
	
	public static DaoProvider getInstance(Context context){
		return new DaoProvider(context);
	}

	public Dao<Rota, Long> getRotaDAO() throws SQLException{
		if(rotaDao == null){
			rotaDao= databaseHelper.getDao(Rota.class);

		}
		return rotaDao;
	}

	public Dao<Ponto, Long> getPontoDao() throws SQLException {
		if(pontoDao == null){
			pontoDao = databaseHelper.getDao(Ponto.class);
		}
		return pontoDao;
	}

	public Dao<Motorista, Long> getMotoristaDao() throws SQLException {
		if(motoristaDao == null){
			motoristaDao = databaseHelper.getDao(Motorista.class);
		}
		return motoristaDao;
	}

	public Dao<Evento, Long> getEventoDao() throws SQLException {
		if(eventoDao == null){
			eventoDao = databaseHelper.getDao(Evento.class);
		}
		
		return eventoDao;
	}

	public Dao<MensagemPre, Long> getMensagemPreDao() throws SQLException {
		if(mensagemPreDao == null){
			mensagemPreDao = databaseHelper.getDao(MensagemPre.class);
		}
		
		return mensagemPreDao;
	}

	public Dao<Message, Long> getMessageDao() throws SQLException {
		if(messageDao == null){
			messageDao = databaseHelper.getDao(Message.class);
		}
		
		return messageDao;
	}

	public Dao <Configuracao, Long> getConfiguracaoDao() throws SQLException {
		if(configuracaoDao == null){
			configuracaoDao = databaseHelper.getDao(Configuracao.class);
		}
		
		return configuracaoDao;
	}

	public Dao <TrigeredEvent, Long> getTrigeredEvenDao() throws SQLException{
		if(trigeredEventDao == null){
			trigeredEventDao = databaseHelper.getDao(TrigeredEvent.class);
		}
		
		return trigeredEventDao;
	}
	
	public Dao <SerialMessage, Long> getSerialMessageDAO() throws Exception{
		if(serialMessageDao == null){
			
			serialMessageDao = databaseHelper.getDao(SerialMessage.class);
		}
		
		return serialMessageDao;
	}
	
	
	public Dao <RotaTransientOperation, Long> getRTODAO() throws SQLException{
		if(rTODAO == null){
			
			rTODAO = databaseHelper.getDao(RotaTransientOperation.class);
		}
		
		return rTODAO;
	}
	
	public static void cleanData(Context context){
		try {
			getInstance(context).getTrigeredEvenDao().deleteBuilder().delete();
			getInstance(context).getMensagemPreDao().deleteBuilder().delete();
			getInstance(context).getMessageDao().deleteBuilder().delete();
			getInstance(context).getMotoristaDao().deleteBuilder().delete();
			getInstance(context).getRotaDAO().deleteBuilder().delete();
			getInstance(context).getEventoDao().deleteBuilder().delete();
			getInstance(context).getConfiguracaoDao().deleteBuilder().delete();
			getInstance(context).getPontoDao().deleteBuilder().delete();
		} catch (SQLException e) {
			LogUtil.e("erro ao apagar os registros do banco");
		}
		
	}
	
}
