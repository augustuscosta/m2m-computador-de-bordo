package com.m2msolutions.m2mcomputadordebordo.database;

import java.sql.SQLException;
import java.util.List;

import android.content.Context;

import com.j256.ormlite.stmt.QueryBuilder;
import com.m2msolutions.m2mcomputadordebordo.model.Ponto;
import com.m2msolutions.m2mcomputadordebordo.model.Rota;
import com.m2msolutions.m2mcomputadordebordo.util.LogUtil;

public class RotaDAO extends DaoProvider {
	
	public RotaDAO(Context context) {
		super(context);
	}

	public List<Rota> getRotasList(){
		try {
			final List<Rota> rotas =getRotaDAO().queryForAll();
			return rotas;
		} catch (SQLException e) {
			LogUtil.e("erro ao extrair rotas do banco de dadods", e);
			return null;
		}finally{
			try {
				getRotaDAO().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro ao fechar cursor das rotas e pontos", e);
			}
		}
	}
	
	public List<Rota> getRotasList(String search){
		try {
			QueryBuilder<Rota, Long> sqlBuilder = getRotaDAO().queryBuilder();
			sqlBuilder.where().like("nome","%"+ search+"%");
			List<Rota> rotas =sqlBuilder.query();
			return rotas;
		} catch (SQLException e) {
			LogUtil.e("erro ao extrair rotas do banco de dadods", e);
			return null;
		}finally{
			try {
				getRotaDAO().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro ao fechar cursor das rotas e pontos", e);
			}
		}
	}
	
	
	public Rota findRota(long id){
		try {
			Rota rota = getRotaDAO().queryForId(id);
			if(rota!= null){
				List<Ponto>  pontos = getPontoDao().queryForEq("id_rota", id );
				rota.setPontos(pontos);
			}
			return rota;
		} catch (SQLException e) {
			LogUtil.e("erro ao pegar a da rota" +id, e);
			return null;
		}finally{
			try {
				getRotaDAO().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro ao fechar cursor da rota e dos pontos", e);
			}
		}
	}
	
		
	
	public void persist(List<Rota> rotas){
		if(rotas== null || rotas.isEmpty()){
			return;
		}
		for (Rota rota : rotas) {
			persist(rota);
		 }	
	}

	public void persist(Rota rota) {
		try {
			getRotaDAO().createOrUpdate(rota);
		} catch (SQLException e) {
			LogUtil.e("erro ao criar rota", e);
		}
	}
	
	public void deleteAll(){
		try {
			getRotaDAO().deleteBuilder().delete();
		} catch (SQLException e) {
			LogUtil.e("erro ao apagar eventos no banco de dados", e);
		}
	}

}
