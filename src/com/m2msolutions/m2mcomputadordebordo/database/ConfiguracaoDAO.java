package com.m2msolutions.m2mcomputadordebordo.database;

import java.sql.SQLException;
import java.util.List;

import android.content.Context;

import com.m2msolutions.m2mcomputadordebordo.model.Configuracao;
import com.m2msolutions.m2mcomputadordebordo.util.LogUtil;

public class ConfiguracaoDAO extends DaoProvider {

	public ConfiguracaoDAO(Context context) {
		super(context);
	}

	public  Configuracao get() {
		try {
			return getConfiguracaoDao().queryForId((long)1);
		} catch (SQLException e) {
			LogUtil.e("erro ao extrair  Mensagens", e);
			return null;
		}finally{
			try {
				getMessageDao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro ao fechar cursor", e);
			}
		}
	}

	

	public  void persist(List<Configuracao> configuracaos) {
		if (configuracaos == null || configuracaos.isEmpty()) {
			return;
		}

		for (Configuracao configuracao: configuracaos) {
			persist(configuracao);
		}
	}

	public void persist(Configuracao configuracao) {
		if (configuracao == null) {
			return;
		}
		try {
			getConfiguracaoDao().createOrUpdate(configuracao);
		} catch (SQLException e) {
			LogUtil.e("erro ao criar Mensagem", e);
		}finally{
			try {
				getMessageDao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro ao fechar cursor", e);
			}
		}
	}

}
