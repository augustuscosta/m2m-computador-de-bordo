package com.m2msolutions.m2mcomputadordebordo.database;

import java.sql.SQLException;
import java.util.List;

import android.content.Context;

import com.m2msolutions.m2mcomputadordebordo.model.Evento;
import com.m2msolutions.m2mcomputadordebordo.util.LogUtil;

public class EventoDAO extends DaoProvider{

	public EventoDAO(Context context) {
		super(context);
	}
	
	public List<Evento> getAll(){
		try {
			return getEventoDao().queryForAll();
		} catch (SQLException e) {
			LogUtil.e("erro ao pegar eventos do banco de dados", e);
			return null;
		}finally{
			 try {
				getEventoDao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro ao fechar cursor", e);
			}
		}
	}
	
	public Evento find(long id){
		try {
			return getEventoDao().queryForId(id);
		} catch (SQLException e) {
			LogUtil.e("erro ao pegar eventos banco de dados" + id, e);
			return null;
		}finally{
			 try {
				getEventoDao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro ao fechar cursor", e);
			}
		}
	}
	
	public void persist(List<Evento> eventos){
		if (eventos == null || eventos.isEmpty()) {
			return;
		}
		
		for (Evento evento : eventos) {
			persist(evento);
		}
	}

	private void persist(Evento evento) {
		if(evento == null){
			return;
		}
		
		try {
			getEventoDao().createOrUpdate(evento);
		} catch (SQLException e) {
			LogUtil.e("erro ao gravar evento no banco de dados", e);
		}
	}

}
