package com.m2msolutions.m2mcomputadordebordo.database;


import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import com.m2msolutions.m2mcomputadordebordo.model.Configuracao;
import com.m2msolutions.m2mcomputadordebordo.model.Evento;
import com.m2msolutions.m2mcomputadordebordo.model.MensagemPre;
import com.m2msolutions.m2mcomputadordebordo.model.Message;
import com.m2msolutions.m2mcomputadordebordo.model.Motorista;
import com.m2msolutions.m2mcomputadordebordo.model.Ponto;
import com.m2msolutions.m2mcomputadordebordo.model.Rota;
import com.m2msolutions.m2mcomputadordebordo.model.RotaTransientOperation;
import com.m2msolutions.m2mcomputadordebordo.model.SerialMessage;
import com.m2msolutions.m2mcomputadordebordo.model.TrigeredEvent;
import com.m2msolutions.m2mcomputadordebordo.model.VehicleStatus;

public class DatabaseConfigUtil extends OrmLiteConfigUtil {

	private static final Class<?>[] classes = new Class[] {
		Motorista.class,Configuracao.class,Rota.class,
		Ponto.class,Message.class,Evento.class,MensagemPre.class
		, TrigeredEvent.class,VehicleStatus.class, SerialMessage.class,
		RotaTransientOperation.class
	};

	public static void main(String[] args) throws Exception {
		writeConfigFile("ormlite_config.txt", classes);
	} 
}