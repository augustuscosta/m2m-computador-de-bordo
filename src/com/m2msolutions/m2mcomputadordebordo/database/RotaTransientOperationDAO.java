package com.m2msolutions.m2mcomputadordebordo.database;

import java.sql.SQLException;
import java.util.List;

import android.content.Context;

import com.m2msolutions.m2mcomputadordebordo.model.MessageOwner;
import com.m2msolutions.m2mcomputadordebordo.model.RotaTransientOperation;
import com.m2msolutions.m2mcomputadordebordo.util.LogUtil;

public class RotaTransientOperationDAO extends DaoProvider{
	
	
	public RotaTransientOperationDAO(Context context) {
		super(context);
	}

	public  List<RotaTransientOperation> getAll() {
		try {
			return getRTODAO().queryForAll();
		} catch (SQLException e) {
			LogUtil.e("erro ao extrair  Operacoes de Rota", e);
			return null;
		}finally{
			try {
				getRTODAO().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro ao fechar cursor", e);
			}
		}
	}
	public  List<RotaTransientOperation> getAllFromServer() {
		try {
			return getRTODAO().queryForEq("messageOwner", MessageOwner.OPERDADOR);
		} catch (SQLException e) {
			LogUtil.e("erro ao extrair  Operacoes de Rota", e);
			return null;
		}finally{
			try {
				getRTODAO().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro ao fechar cursor", e);
			}
		}
	}

	public  RotaTransientOperation find(long id) {
		try {
			return getRTODAO().queryForId(id);
		} catch (SQLException e) {
			LogUtil.e("erro ao extrair  Operacoes de Rota", e);
			return null;
		}finally{
			try {
				getRTODAO().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro ao fechar cursor", e);
			}
		}
	}

	public  void persist(List<RotaTransientOperation> rtos) {
		if (rtos == null || rtos.isEmpty()) {
			return;
		}

		for (RotaTransientOperation rto : rtos) {
			persist(rto);
		}
	}

	public void persist(RotaTransientOperation rto) {
		if (rto == null) {
			return;
		}
		try {
			getRTODAO().createOrUpdate(rto);
		} catch (SQLException e) {
			LogUtil.e("erro ao criar Operacoes de Rota", e);
		}finally{
			try {
				getRTODAO().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro ao fechar cursor", e);
			}
		}
	}
	
	public void deleteAll(){
		try {
			getRTODAO().deleteBuilder().delete();
		} catch (SQLException e) {
			LogUtil.e("erro ao apagar Operacoes de Rota", e);
		} catch (Exception e) {
			LogUtil.e("erro ao apagar Operacoes de Rota", e);
		}
	}

}
