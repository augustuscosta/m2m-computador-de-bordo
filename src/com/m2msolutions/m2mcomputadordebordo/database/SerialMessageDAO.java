package com.m2msolutions.m2mcomputadordebordo.database;

import java.sql.SQLException;
import java.util.List;

import android.content.Context;

import com.m2msolutions.m2mcomputadordebordo.model.SerialMessage;
import com.m2msolutions.m2mcomputadordebordo.util.LogUtil;

public class SerialMessageDAO extends DaoProvider {

	public SerialMessageDAO(Context context) {
		super(context);
	}

	public List<SerialMessage> getAll(){
		try {
			return getSerialMessageDAO().queryForAll();
		} catch (Exception e) {
			LogUtil.e("erro ao pegar eventos do banco de dados", e);
			return null;
		}finally{
			try {
				getEventoDao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro ao fechar cursor", e);
			}
		}
	}

	public List<SerialMessage> getAllUnsent(){
		try {
			return getSerialMessageDAO().queryForEq("sent","0");
		} catch (Exception e) {
			LogUtil.e("erro ao pegar eventos do banco de dados", e);
			return null;
		}finally{
			try {
				getEventoDao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro ao fechar cursor", e);
			}
		}
	}

	
	public void persist(SerialMessage command){
		try {
			getSerialMessageDAO().create(command);
		}catch (Exception e) {
			LogUtil.e("erro ao fechar cursor", e);
		}
	}
	
	public void update(SerialMessage command) throws SQLException, Exception{
		getSerialMessageDAO().update(command);
	}
	
}
