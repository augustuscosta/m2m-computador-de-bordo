package com.m2msolutions.m2mcomputadordebordo.database;

import java.sql.SQLException;
import java.util.List;

import android.content.Context;

import com.m2msolutions.m2mcomputadordebordo.model.Ponto;
import com.m2msolutions.m2mcomputadordebordo.util.LogUtil;

public class PontoDao extends DaoProvider {

	public PontoDao(Context context) {
		super(context);
	}
	
	public List<Ponto> ListAll() {
		try {
			return getPontoDao().queryForAll();
		} catch (SQLException e) {
			LogUtil.e("erro ao extrair rotas do banco de dadods", e);
			return null;
		}finally{
			try {
				getPontoDao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro ao fechar cursor dos pontos", e);
			}
		}
	}
	
	public List<Ponto> ListFromRota(long id) {
		try {
			return getPontoDao().queryForEq("id_rota", id);
		} catch (SQLException e) {
			LogUtil.e("erro ao extrair rotas do banco de dados da rota" + id, e);
			return null;
		}finally{
			try {
				getPontoDao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro ao fechar cursor dos pontos", e);
			}
		}
	}
	
	public Ponto find(long id) {
		try {
			return getPontoDao().queryForId(id);
		} catch (SQLException e) {
			LogUtil.e("erro ao extrair rotas do banco de dados da rota" + id, e);
			return null;
		}finally{
			try {
				getPontoDao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro ao fechar cursor dos pontos", e);
			}
		}
	}
	
	public void persist(List<Ponto> pontos){
		if(pontos == null || pontos.isEmpty()){
			return; 
		}
		
		for (Ponto ponto : pontos) {
			persist(ponto);
		}
	}

	private void persist(Ponto ponto) {
		try {
			getPontoDao().createOrUpdate(ponto);
		} catch (SQLException e) {
			e.printStackTrace();
			LogUtil.e("erro ao fechar criar ponto", e);
		}
	}
}
