package com.m2msolutions.m2mcomputadordebordo.database;

import java.sql.SQLException;
import java.util.List;

import com.m2msolutions.m2mcomputadordebordo.model.Message;
import com.m2msolutions.m2mcomputadordebordo.model.MessageOwner;
import com.m2msolutions.m2mcomputadordebordo.util.LogUtil;

import android.content.Context;

public class MessageDAO extends DaoProvider {

	public MessageDAO(Context context) {
		super(context);
	}

	public  List<Message> getAll() {
		try {
			return getMessageDao().queryForAll();
		} catch (SQLException e) {
			LogUtil.e("erro ao extrair  Mensagens", e);
			return null;
		}finally{
			try {
				getMessageDao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro ao fechar cursor", e);
			}
		}
	}
	public  List<Message> getAllFromServer() {
		try {
			return getMessageDao().queryForEq("messageOwner", MessageOwner.OPERDADOR);
		} catch (SQLException e) {
			LogUtil.e("erro ao extrair  Mensagens", e);
			return null;
		}finally{
			try {
				getMessageDao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro ao fechar cursor", e);
			}
		}
	}

	public  Message find(long id) {
		try {
			return getMessageDao().queryForId(id);
		} catch (SQLException e) {
			LogUtil.e("erro ao extrair  Mensagens", e);
			return null;
		}finally{
			try {
				getMessageDao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro ao fechar cursor", e);
			}
		}
	}

	public  void persist(List<Message> mensagens) {
		if (mensagens == null || mensagens.isEmpty()) {
			return;
		}

		for (Message message : mensagens) {
			persist(message);
		}
	}

	public void persist(Message message) {
		if (message == null) {
			return;
		}
		try {
			getMessageDao().createOrUpdate(message);
		} catch (SQLException e) {
			LogUtil.e("erro ao criar Mensagem", e);
		}finally{
			try {
				getMessageDao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro ao fechar cursor", e);
			}
		}
	}
	
	public void deleteAll(){
		try {
			getMessageDao().deleteBuilder().delete();
		} catch (SQLException e) {
			LogUtil.e("erro ao apagar Mensagens", e);
		}
	}

}
