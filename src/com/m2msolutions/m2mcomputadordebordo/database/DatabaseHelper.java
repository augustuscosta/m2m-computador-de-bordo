package com.m2msolutions.m2mcomputadordebordo.database;

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.m2msolutions.m2mcomputadordebordo.R;
import com.m2msolutions.m2mcomputadordebordo.model.Configuracao;
import com.m2msolutions.m2mcomputadordebordo.model.Evento;
import com.m2msolutions.m2mcomputadordebordo.model.MensagemPre;
import com.m2msolutions.m2mcomputadordebordo.model.Message;
import com.m2msolutions.m2mcomputadordebordo.model.Motorista;
import com.m2msolutions.m2mcomputadordebordo.model.Ponto;
import com.m2msolutions.m2mcomputadordebordo.model.Rota;
import com.m2msolutions.m2mcomputadordebordo.model.RotaTransientOperation;
import com.m2msolutions.m2mcomputadordebordo.model.SerialMessage;
import com.m2msolutions.m2mcomputadordebordo.model.TrigeredEvent;
import com.m2msolutions.m2mcomputadordebordo.model.VehicleStatus;
import com.m2msolutions.m2mcomputadordebordo.util.LogUtil;


public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
	
	private static final String DATABASE_NAME = "m2mconputadordebordo.db";
	private static final int DATABASE_VERSION = 2;
	private static DatabaseHelper databaseHelper;

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
	}
	
	public static OrmLiteSqliteOpenHelper getDatabase(Context context){
		if(databaseHelper == null){
			databaseHelper = new DatabaseHelper(context);
		}
		
		return databaseHelper;
	}

	
	@Override
	public void onCreate(SQLiteDatabase arg0, ConnectionSource arg1) {
		createOrLoadTables();
		
	}
	

	private void createOrLoadTables() {
		try {
			TableUtils.createTable(connectionSource, Motorista.class);
			TableUtils.createTable(connectionSource, Ponto.class);
			TableUtils.createTable(connectionSource, Rota.class);
			TableUtils.createTable(connectionSource, Evento.class);
			TableUtils.createTable(connectionSource, Configuracao.class);
			TableUtils.createTable(connectionSource, Message.class);
			TableUtils.createTable(connectionSource, MensagemPre.class);
			TableUtils.createTable(connectionSource, TrigeredEvent.class);
			TableUtils.createTable(connectionSource, VehicleStatus.class);
			TableUtils.createTable(connectionSource, SerialMessage.class);
			TableUtils.createTable(connectionSource, RotaTransientOperation.class);
		} catch (SQLException e) {
			LogUtil.e(DatabaseHelper.class.getName(), e);
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, ConnectionSource arg1, int arg2,
			int arg3) {
		// TODO Auto-generated method stub
		
	}

}
