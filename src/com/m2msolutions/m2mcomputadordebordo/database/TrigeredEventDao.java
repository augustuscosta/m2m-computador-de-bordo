package com.m2msolutions.m2mcomputadordebordo.database;

import java.sql.SQLException;
import java.util.List;

import android.content.Context;

import com.m2msolutions.m2mcomputadordebordo.model.TrigeredEvent;
import com.m2msolutions.m2mcomputadordebordo.util.LogUtil;

public class TrigeredEventDao extends DaoProvider{

	public TrigeredEventDao(Context context) {
		super(context);
	}
	
	public List<TrigeredEvent> getAll(){
		try {
			return addEvents(getTrigeredEvenDao().queryForAll());
		} catch (SQLException e) {
			LogUtil.e("erro ao pegar eventos do banco de dados", e);
			return null;
		}finally{
			 try {
				 getTrigeredEvenDao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro ao fechar cursor", e);
			}
		}
	}
	
	private List<TrigeredEvent> addEvents(List<TrigeredEvent> queryForAll) throws SQLException {
		for (TrigeredEvent trigeredEvent : queryForAll) {
			trigeredEvent.setEvento(getEventoDao().queryForId(trigeredEvent.getEventoId()));
		}
		return queryForAll;
	}

	public TrigeredEvent find(long id){
		try {
			return getTrigeredEvenDao().queryForId(id);
		} catch (SQLException e) {
			LogUtil.e("erro ao pegar eventos banco de dados" + id, e);
			return null;
		}finally{
			 try {
				 getTrigeredEvenDao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro ao fechar cursor", e);
			}
		}
	}
	
	public void persist(List<TrigeredEvent> eventos){
		if (eventos == null || eventos.isEmpty()) {
			return;
		}
		
		for (TrigeredEvent evento : eventos) {
			persist(evento);
		}
	}

	public void persist(TrigeredEvent evento) {
		if(evento == null){
			return;
		}
		
		try {
			getTrigeredEvenDao().createOrUpdate(evento);
		} catch (SQLException e) {
			LogUtil.e("erro ao gravar evento no banco de dados", e);
		}
	}
	
	public void deleteAll(){
		try {
			getTrigeredEvenDao().deleteBuilder().delete();
		} catch (SQLException e) {
			LogUtil.e("erro ao apagar eventos no banco de dados", e);
		}
	}

}
