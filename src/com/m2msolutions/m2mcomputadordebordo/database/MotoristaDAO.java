package com.m2msolutions.m2mcomputadordebordo.database;

import java.sql.SQLException;
import java.util.List;

import android.content.Context;

import com.m2msolutions.m2mcomputadordebordo.model.Motorista;
import com.m2msolutions.m2mcomputadordebordo.util.LogUtil;

public class MotoristaDAO extends DaoProvider {

	public MotoristaDAO(Context context) {
		super(context);
	}

	public List<Motorista> listAll(){
		try {
			return getMotoristaDao().queryForAll();
		} catch (SQLException e) {
			LogUtil.e("erro ao pegar motoristas do banco de dados", e);
			return null;
		}finally{
			try {
				getMotoristaDao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro fechar cursor", e);
			}
		}
	}

	public Motorista fing(Long id){
		if(id == null) return null;
		try {
			return getMotoristaDao().queryForId(id);
		} catch (SQLException e) {
			LogUtil.e("erro ao pegar motoristas do banco de dados", e);
			return null;
		}finally{
			try {
				getMotoristaDao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro fechar cursor", e);
			}
		}
	}

	public Motorista log(Motorista motorista){
		try {
			
			List<Motorista> motoristas= getMotoristaDao().queryBuilder().where().eq("nome", motorista.getNome())
					.and().eq("senha", motorista.getSenha()).query();
			if(motoristas != null && !motoristas.isEmpty()){
				return motoristas.get(0);				
			}else{
				return null;
			}
		} catch (SQLException e) {
			LogUtil.e("erro ao pegar motoristas do banco de dados", e);
			return null;
		}finally{
			try {
				getMotoristaDao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro fechar cursor", e);
			}
		}
	}

	public void persist(List<Motorista> motoristas){
		if(motoristas == null || motoristas.isEmpty()){
			return;
		}

		for (Motorista motorista : motoristas) {
			persist(motorista);
		}
	}

	public void persist(Motorista motorista) {
		if(motorista == null){
			return;
		}

		try {
			getMotoristaDao().createOrUpdate(motorista);
		} catch (SQLException e) {
			LogUtil.e("erro ao persistir motorista", e);
		}
	}
	
	public void deleteAll(){
		try {
			getMotoristaDao().deleteBuilder().delete();
		} catch (SQLException e) {
			LogUtil.e("erro ao apagar eventos no banco de dados", e);
		}
	}

}
