
package com.m2msolutions.m2mcomputadordebordo;

import java.io.File;
import java.io.IOException;
import java.security.InvalidParameterException;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.serialport.SerialPort;
import android.serialport.SerialPortFinder;

import com.m2msolutions.m2mcomputadordebordo.database.DatabaseHelper;
import com.m2msolutions.m2mcomputadordebordo.util.LogUtil;

public class Application extends android.app.Application {

	@Override
	public void onCreate() {
		super.onCreate();
		LogUtil.initLog(this, "M2M COMPUTADOR DE BORDO");
		DatabaseHelper.getDatabase(this);
	}


	public SerialPortFinder mSerialPortFinder = new SerialPortFinder();
	private SerialPort mSerialPort = null;

	public SerialPort getSerialPort() throws SecurityException, IOException, InvalidParameterException {
		if (mSerialPort == null) {
			/* Read serial port parameters */
			SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
			String path = sp.getString("DEVICE", "/dev/ttyS1");
			int baudrate = Integer.decode(sp.getString("BAUDRATE", "38400"));
			Editor editor = sp.edit();
			editor.putString("DEVICE", path);
			editor.putString("BAUDRATE", Integer.toString(baudrate));
			editor.commit();

			/* Check parameters */
			if ( (path.length() == 0) || (baudrate == -1)) {
				throw new InvalidParameterException(); 
			}

			/* Open the serial port */
			mSerialPort = new SerialPort(new File(path), baudrate);
		}
		return mSerialPort;
	}


	public void closeSerialPort() {
		if (mSerialPort != null) {
			mSerialPort.close();
			mSerialPort = null;
		}
	}
}
